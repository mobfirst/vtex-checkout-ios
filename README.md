# mobfirst-vtex-checkout-pod

[![CI Status](http://img.shields.io/travis/Diego Merks/mobfirst-vtex-checkout-pod.svg?style=flat)](https://travis-ci.org/Diego Merks/mobfirst-vtex-checkout-pod)
[![Version](https://img.shields.io/cocoapods/v/mobfirst-vtex-checkout-pod.svg?style=flat)](http://cocoapods.org/pods/mobfirst-vtex-checkout-pod)
[![License](https://img.shields.io/cocoapods/l/mobfirst-vtex-checkout-pod.svg?style=flat)](http://cocoapods.org/pods/mobfirst-vtex-checkout-pod)
[![Platform](https://img.shields.io/cocoapods/p/mobfirst-vtex-checkout-pod.svg?style=flat)](http://cocoapods.org/pods/mobfirst-vtex-checkout-pod)

## Get payment methods example

```objc
PaymentApi *paymentApi = [[PaymentApi alloc] initWithAccountName:@"mobfirst" vtexApiKey:@"vtexappkey-mobfirst-NVETAF" andVtexApiToken:@"CPAUGHCBWJHXOIOJSXEHKSBKHCEEYXSFTCHQQCUCVRYJITPRCMDDTREJOPMECYDZRCDLAXTPCBMSURCNNNBLTOLCYYCAQQZXOYSGQDQETOASFMPWNZQPWSGCQBNCVTZM"];
    
[paymentApi getPaymentMethodsWithCompletionHandler:^(PaymentMethodsReturnObject *paymentMethodsReturnObject, ErrorType error) {
    
    if(error == NONE) {
        printf("ok");
    }
    else {
        printf("error");
    }
}];
```

## Get installment options example
```objc
PaymentApi *paymentApi = [[PaymentApi alloc] initWithAccountName:@"mobfirst" vtexApiKey:@"vtexappkey-mobfirst-NVETAF" andVtexApiToken:@"CPAUGHCBWJHXOIOJSXEHKSBKHCEEYXSFTCHQQCUCVRYJITPRCMDDTREJOPMECYDZRCDLAXTPCBMSURCNNNBLTOLCYYCAQQZXOYSGQDQETOASFMPWNZQPWSGCQBNCVTZM"];
    
[paymentApi getInstallmentsWithPaymentSystem:@"2" value:20000 andCompletionHandler:^(PaymentApiInstallmentsReturnObject *paymentApiInstallmentsReturnObject, ErrorType error) {
    
    if(error == NONE) {
        printf("ok");
    }
    else {
        printf("error");
    }
}];
```

## Checkout example

```objc
CheckoutApi *checkoutApi = [[CheckoutApi alloc] initWithAccountName:@"mobfirst" vtexApiKey:@"vtexappkey-mobfirst-NVETAF" andVtexApiToken:@"CPAUGHCBWJHXOIOJSXEHKSBKHCEEYXSFTCHQQCUCVRYJITPRCMDDTREJOPMECYDZRCDLAXTPCBMSURCNNNBLTOLCYYCAQQZXOYSGQDQETOASFMPWNZQPWSGCQBNCVTZM"];
    
PaymentApi *paymentApi = [[PaymentApi alloc] initWithAccountName:@"mobfirst" vtexApiKey:@"vtexappkey-mobfirst-NVETAF" andVtexApiToken:@"CPAUGHCBWJHXOIOJSXEHKSBKHCEEYXSFTCHQQCUCVRYJITPRCMDDTREJOPMECYDZRCDLAXTPCBMSURCNNNBLTOLCYYCAQQZXOYSGQDQETOASFMPWNZQPWSGCQBNCVTZM"];

NSArray *items = [[NSArray alloc] initWithObjects:[[Item alloc] initWithId:@"1" quantity:1 andSeller:@"1"], nil];

[checkoutApi simulateWithItems:items postalCode:@"81270010" country:@"BRA" andCompletionHandler:^(SimulationReturnObject *simulationReturnObject, ErrorType error) {
    
    if(error == NONE) {
        
        UserProfile *userProfile = [[UserProfile alloc] initWithJsonString:@"{\r\n    \"email\": \"suporte@mobfirst.com\",\r\n    \"firstName\": \"Teste\",\r\n    \"lastName\": \"Teste\",\r\n    \"document\": \"31528569032\",\r\n    \"documentType\": \"cpf\",\r\n    \"phone\": \"1234567890\",\r\n    \"corporateName\": null,\r\n    \"tradeName\": null,\r\n    \"corporateDocument\": null,\r\n    \"stateInscription\": null,\r\n    \"corporatePhone\": null,\r\n    \"isCorporate\": false,\r\n    \"expectedOrderFormSections\": []\r\n  }"];
        
        UserAddress *userAddress = [[UserAddress alloc] initWithJsonString:@"{\r\n      \"addressType\": \"residential\",\r\n      \"receiverName\": \"Teste\",\r\n      \"addressId\": \"\",\r\n      \"postalCode\": \"22022032\",\r\n      \"city\": \"Rio de Janeiro\",\r\n      \"state\": \"RJ\",\r\n      \"country\": \"BRA\",\r\n      \"street\": \"Rua da minha casa\",\r\n      \"number\": \"12345\",\r\n      \"neighborhood\": \"Copacabana\",\r\n      \"complement\": \"15\u00BA andar\",\r\n      \"reference\": \"\",\r\n      \"geoCoordinates\": []\r\n    }"];
        
        SimulationReturnObject *newSimulationReturnObject = [[simulationReturnObject selectSla:@"Normal" forItemIndex:0] selectInstallmentsWithPaymentSystem:@"6" count:1];
        
        [checkoutApi placeOrderWithSimulationReturnObject:newSimulationReturnObject userProfile:userProfile userAddress:userAddress andCompletionHandler:^(PlaceOrderReturnObject *placeOrderReturnObject, ErrorType error) {
            
            if(error == NONE) {
                
                CreditCard *card = [[CreditCard alloc] initWithHolderName:@"Teste Teste" cardNumber:@"4929606150057499" validationCode:@"471" dueDate:@"06/19" document:@"" accountId:@"" address:@"" callbackUrl:@""];
                
                [paymentApi sentPaymentWithPlaceOrderReturnObject:placeOrderReturnObject creditCard:card andCompletionHandler:^(ErrorType error) {
                    
                    if(error == NONE) {
                        printf("ok");
                    }
                    else {
                        printf("error");
                    }
                }];
            }
            else {
                printf("error");
            }
        }];
    }
    else {
        printf("error");
    }
}];
```

## One click checkout example

```objc
CheckoutApi *checkoutApi = [[CheckoutApi alloc] initWithAccountName:@"mobfirst" vtexApiKey:@"vtexappkey-mobfirst-NVETAF" andVtexApiToken:@"CPAUGHCBWJHXOIOJSXEHKSBKHCEEYXSFTCHQQCUCVRYJITPRCMDDTREJOPMECYDZRCDLAXTPCBMSURCNNNBLTOLCYYCAQQZXOYSGQDQETOASFMPWNZQPWSGCQBNCVTZM"];
    
NSArray *items = [[NSArray alloc] initWithObjects:[[Item alloc] initWithId:@"1" quantity:1 andSeller:@"1"], nil];

[checkoutApi oneClickCheckoutWithItems:items andCompletionHandler:^(ErrorType error) {
    
    if(error == NONE) {
        printf("ok");
    }
    else {
        printf("error");
    }
}];
```

## Requirements

## Installation

mobfirst-vtex-checkout-pod is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'mobfirst-vtex-checkout-pod'
```

## Author

Diego Merks, merks@mobfirst.com

## License

mobfirst-vtex-checkout-pod is available under the MIT license. See the LICENSE file for more info.
