//
//  Installment.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 28/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Installment : NSObject

@property (nonatomic) int count;
@property (nonatomic) BOOL hasInterestRate;
@property (nonatomic) long interestRate;
@property (nonatomic) long value;
@property (nonatomic) long total;
@property (nonatomic, strong) NSMutableArray *sellerMerchantInstallments;

- (id) initWithCount:(int) count hasInterestRate:(BOOL) hasInterestRate interestRate:(long) interestRate value:(long) value total:(long) total andSellerMerchantInstallments:(NSMutableArray *) sellerMerchantInstallments;
- (id) initWithJsonString:(NSString *) jsonString;

@end
