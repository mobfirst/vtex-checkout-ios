//
//  JsonHelper.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 30/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JsonHelper : NSObject

+ (BOOL) getDictionary:(NSDictionary **) dictionary fromJsonString:(NSString *) jsonString;
+ (BOOL) getJsonData:(NSData **) jsonData fromDictionary:(NSDictionary *) dictionary withJsonKey:(NSString *) key;
+ (BOOL) getJsonData:(NSData **) jsonData fromArray:(NSArray *) array;
+ (BOOL) getJsonData:(NSData **) jsonData fromDictionary:(NSDictionary *) dictionary;
+ (BOOL) getJsonArray:(NSArray **) jsonArray fromJsonData:(NSData *) jsonData;

@end
