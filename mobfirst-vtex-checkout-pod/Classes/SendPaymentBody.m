//
//  SendPaymentBody.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 30/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "SendPaymentBody.h"
#import "MerchantTransaction.h"
#import "Payment.h"
#import "JsonKeys.h"

@implementation SendPaymentBody

- (id) initWithPlaceOrderReturnObject:(PlaceOrderReturnObject *) placeOrderReturnObject andCreditCard:(CreditCard *) creditCard {
    
    if(self = [super init]) {
        
        self.placeOrderReturnObject = placeOrderReturnObject;
        self.creditCard = creditCard;
    }
    
    return self;
}

- (NSArray *) toJsonObject {
    
    NSMutableDictionary *jsonObject = [[NSMutableDictionary alloc] init];
    if([[[[self placeOrderReturnObject] transactionData] merchantTransactions] count] == 1) {
        
        MerchantTransaction *merchantTransaction = [[[[self placeOrderReturnObject] transactionData] merchantTransactions] objectAtIndex:0];
        if([[merchantTransaction payments] count] == 1) {
            
            Payment *payment = [[merchantTransaction payments] objectAtIndex:0];
            
            [jsonObject setObject:[NSNumber numberWithInteger:[[payment paymentSystem] integerValue]] forKey:_sendPaymentSystem];
            [jsonObject setObject:[NSNumber numberWithInt:[payment installmentCount]] forKey:_sendPaymentInstallments];
            [jsonObject setObject:[NSNumber numberWithLong:[payment value]] forKey:_sendPaymentValue];
            [jsonObject setObject:[NSNumber numberWithLong:[payment interestRate]] forKey:_sendPaymentInstallmentsInterestRate];
            [jsonObject setObject:[NSNumber numberWithLong:[payment value] / [payment installmentCount]] forKey:_sendPaymentInstallmentsValue];
            [jsonObject setObject:[NSNumber numberWithLong:[payment referenceValue]] forKey:_sendPaymentReferenceValue];
        }
        
        [jsonObject setObject:[merchantTransaction toJsonObject] forKey:_sendPaymentTransaction];
    }
    
    if([self creditCard]) [jsonObject setObject:[[self creditCard] toJsonObject] forKey:_sendPaymentFields];
    return [[NSArray alloc] initWithObjects:jsonObject, nil];
}

@end
