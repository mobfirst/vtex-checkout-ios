//
//  Payment.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 29/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Payment : NSObject

@property (nonatomic, strong) NSString *paymentSystem;
@property (nonatomic) long value;
@property (nonatomic) long referenceValue;
@property (nonatomic) int installmentCount;
@property (nonatomic) long interestRate;

- (id) initWithPaymentSystem:(NSString *) paymentSystem value:(long) value referenceValue:(long) referenceValue installmentCount:(int) installmentCount andInterestRate:(long) interestRate;
- (id) initWithJsonString:(NSString *) jsonString;

- (void) setInstallmentCount:(int) installmentCount;
- (void) setInterestRate:(long) interestRate;

@end
