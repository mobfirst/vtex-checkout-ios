//
//  UserAddresses.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 30/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserAddresses : NSObject

@property (nonatomic, strong) NSMutableArray *addresses;

- (id) initWithUserAddresses:(NSMutableArray *) addresses;
- (id) initWithJsonString:(NSString *) jsonString;

@end
