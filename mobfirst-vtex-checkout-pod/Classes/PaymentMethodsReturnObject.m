//
//  PaymentMethodsReturnObject.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 06/11/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "PaymentMethodsReturnObject.h"
#import "JsonKeys.h"
#import "JsonHelper.h"
#import "PaymentMethod.h"

@implementation PaymentMethodsReturnObject

- (id) initWithJsonString:(NSString *) jsonString {
    
    NSDictionary *jsonDictionary;
    NSData *jsonData;
    NSArray *jsonArray;
    
    self.paymentMethods = [[NSMutableArray alloc] init];
    
    if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
    if(![JsonHelper getJsonData:&jsonData fromDictionary:jsonDictionary]) return self;
    if(![JsonHelper getJsonArray:&jsonArray fromJsonData:jsonData]) return self;
    
    for(NSDictionary *item in jsonArray) {
        
        NSData *jsonData;
        if(![JsonHelper getJsonData:&jsonData fromDictionary:item]) continue;
        
        [self.paymentMethods addObject:[[PaymentMethod alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]]];
    }
    
    return self;
}

@end
