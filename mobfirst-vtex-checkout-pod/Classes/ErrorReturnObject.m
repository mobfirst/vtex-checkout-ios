//
//  ErrorReturnObject.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 28/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "ErrorReturnObject.h"
#import "JsonKeys.h"
#import "JsonHelper.h"

@implementation ErrorReturnObject

- (id) initWithError:(Error *) error {
    
    if(self = [super init]) {
        self.error = error;
    }
    
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        NSData *jsonData;
        
        if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        if(![JsonHelper getJsonData:&jsonData fromDictionary:jsonDictionary withJsonKey:_errorReturnObjectError]) return self;
        
        self.error = [[Error alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
    }
    
    return self;
}

@end
