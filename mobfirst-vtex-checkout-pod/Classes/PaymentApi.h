//
//  PaymentApi.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 28/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SimulationReturnObject.h"
#import "PlaceOrderReturnObject.h"
#import "CreditCard.h"
#import "ErrorType.h"
#import "PaymentApiInstallmentsReturnObject.h"
#import "PaymentMethodsReturnObject.h"

@interface PaymentApi : NSObject

@property (nonatomic, strong) NSDictionary *headers;
@property (nonatomic, strong) NSString *accountName;
@property (nonatomic, strong) NSString *vtexApiKey;
@property (nonatomic, strong) NSString *vtexApiToken;

- (id) initWithAccountName:(NSString *) acoountName vtexApiKey:(NSString *) vtexApiKey andVtexApiToken:(NSString *) vtexApiToken;

- (void) getPaymentMethodsWithCompletionHandler:(void (^)(PaymentMethodsReturnObject *paymentMethodsReturnObject, ErrorType error)) completionHandler;
- (void) getInstallmentsWithPaymentSystem:(NSString *) paymentSystem value:(long) value andCompletionHandler:(void (^)(PaymentApiInstallmentsReturnObject *paymentApiInstallmentsReturnObject, ErrorType error)) completionHandler;
- (void) getValueWithInterestFromSimulationReturnObject:(SimulationReturnObject *) simulationReturnObject withCompletionHandler:(void (^)(long valueWithInterest, long referenceValue, ErrorType error)) completionHandler;
- (void) sentPaymentWithPlaceOrderReturnObject:(PlaceOrderReturnObject *) placeOrderReturnObject creditCard:(CreditCard *) creditCard andCompletionHandler:(void (^)(ErrorType error)) completionHandler;

@end
