//
//  CheckoutApi.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 28/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HttpCheckoutConnection.h"
#import "SimulationReturnObject.h"
#import "UserProfile.h"
#import "UserAddress.h"
#import "PlaceOrderReturnObject.h"
#import "ErrorType.h"

@interface CheckoutApi : NSObject

@property (nonatomic, strong) NSDictionary *headers;
@property (nonatomic, strong) NSString *accountName;
@property (nonatomic, strong) NSString *vtexApiKey;
@property (nonatomic, strong) NSString *vtexApiToken;

- (id) initWithAccountName:(NSString *) acoountName vtexApiKey:(NSString *) vtexApiKey andVtexApiToken:(NSString *) vtexApiToken;

- (void) simulateWithItems:(NSArray *) items postalCode:(NSString *) postalCode country:(NSString *) country andCompletionHandler:(void (^)(SimulationReturnObject *simulationReturnObject, ErrorType error)) completionHandler;
- (void) placeOrderWithSimulationReturnObject:(SimulationReturnObject *) simulationReturnObject userProfile:(UserProfile *) userProfile userAddress:(UserAddress *) userAddress andCompletionHandler:(void (^)(PlaceOrderReturnObject *placeOrderReturnObject, ErrorType error)) completionHandler;
- (void) gatewayCallbackWithPlaceOrderReturnObject:(PlaceOrderReturnObject *) placeOrderReturnObject andCompletionHandler:(void (^)(ErrorType error)) completionHandler;
- (void) oneClickCheckoutWithItems:(NSArray *) items andCompletionHandler:(void (^)(ErrorType error)) completionHandler;

@end
