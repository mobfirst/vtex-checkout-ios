//
//  ErrorType.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 03/11/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#ifndef ErrorType_h
#define ErrorType_h

typedef enum errorType
{
    NONE,
    PARAM_ERROR,
    PARAM_VALIDATION_ERROR,
    JSON_ERROR,
    COMMUNICATION_ERROR,
    RESPONSE_ERROR
} ErrorType;

#endif /* ErrorType_h */
