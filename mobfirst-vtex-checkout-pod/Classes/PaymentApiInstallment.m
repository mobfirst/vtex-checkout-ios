//
//  PaymentApiInstallment.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 30/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "PaymentApiInstallment.h"
#import "JsonKeys.h"
#import "JsonHelper.h"
#import "PaymentApiOption.h"

@implementation PaymentApiInstallment

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        NSData *jsonData;
        NSArray *jsonArray;
        
        self.options = [[NSMutableArray alloc] init];
        
        if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        if(![JsonHelper getJsonData:&jsonData fromDictionary:jsonDictionary withJsonKey:_paymentApiInstallmentOptions]) return self;
        if(![JsonHelper getJsonArray:&jsonArray fromJsonData:jsonData]) return self;
        
        for(NSDictionary *item in jsonArray) {
            
            NSData *jsonData;
            if(![JsonHelper getJsonData:&jsonData fromDictionary:item]) continue;
            
            [self.options addObject:[[PaymentApiOption alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]]];
        }
    }
    
    return self;
}

@end
