//
//  SendPaymentBody.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 30/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlaceOrderReturnObject.h"
#import "CreditCard.h"

@interface SendPaymentBody : NSObject

@property (nonatomic, strong) PlaceOrderReturnObject *placeOrderReturnObject;
@property (nonatomic, strong) CreditCard *creditCard;

- (id) initWithPlaceOrderReturnObject:(PlaceOrderReturnObject *) placeOrderReturnObject andCreditCard:(CreditCard *) creditCard;

- (NSArray *) toJsonObject;

@end
