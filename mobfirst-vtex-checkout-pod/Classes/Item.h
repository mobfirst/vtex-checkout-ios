//
//  Item.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 28/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Item : NSObject

@property (nonatomic, strong) NSString *id;
@property (nonatomic) int quantity;
@property (nonatomic, strong) NSString *seller;
@property (nonatomic) long price;

- (id) initWithId:(NSString *) id quantity:(int) quantity andSeller:(NSString *) seller;
- (id) initWithId:(NSString *) id quantity:(int) quantity seller:(NSString *) seller andPrice:(long) price;
- (id) initWithJsonString:(NSString *) jsonString;

- (NSDictionary *) toJsonObject;

@end
