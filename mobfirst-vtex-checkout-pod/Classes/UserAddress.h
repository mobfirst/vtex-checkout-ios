//
//  UserAddress.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 30/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserAddress : NSObject

@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *addressType;
@property (nonatomic, strong) NSString *receiverName;
@property (nonatomic, strong) NSString *addressId;
@property (nonatomic, strong) NSString *postalCode;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *street;
@property (nonatomic, strong) NSString *number;
@property (nonatomic, strong) NSString *neighborhood;
@property (nonatomic, strong) NSString *complement;
@property (nonatomic, strong) NSString *reference;

- (id) initWithId:(NSString *) id addressType:(NSString *) addressType receiverName:(NSString *) receiverName addressId:(NSString *) addressId postalCode:(NSString *) postalCode city:(NSString *) city state:(NSString *) state country:(NSString *) country street:(NSString *) street number:(NSString *) number neighborhood:(NSString *) neighborhood complement:(NSString *) complement andReference:(NSString *) reference;
- (id) initWithJsonString:(NSString *) jsonString;

- (NSDictionary *) toJsonObject;

@end
