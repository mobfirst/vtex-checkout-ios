//
//  PlaceOrderReturnObject.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 29/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "PlaceOrderReturnObject.h"
#import "JsonKeys.h"
#import "JsonHelper.h"
#import "Order.h"
#import "MerchantTransaction.h"
#import "Payment.h"

@implementation PlaceOrderReturnObject

- (id) initWithTransactionData:(TransactionData *) transactionData andOrders:(NSMutableArray *) orders {
    
    if(self = [super init]) {
        
        self.transactionData = transactionData;
        self.orders = orders;
    }
    
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        NSData *jsonData;
        NSArray *jsonArray;
        
        self.orders = [[NSMutableArray alloc] init];
        
        if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        if(![JsonHelper getJsonData:&jsonData fromDictionary:jsonDictionary withJsonKey:_placeOrderReturnObjectOrders]) return self;
        if(![JsonHelper getJsonArray:&jsonArray fromJsonData:jsonData]) return self;
        
        for(NSDictionary *item in jsonArray) {
            
            NSData *jsonData;
            if(![JsonHelper getJsonData:&jsonData fromDictionary:item]) continue;
            
            [self.orders addObject:[[Order alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]]];
        }
        
        if(![JsonHelper getJsonData:&jsonData fromDictionary:jsonDictionary withJsonKey:_placeOrderReturnObjectTransactionData]) return self;
        self.transactionData = [[TransactionData alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
    }
    
    return self;
}

- (void) setInstallmentData:(int) installmentCount interestRate:(long) interestRate {
    
    if([[[self transactionData] merchantTransactions] count] > 0) {
        
        MerchantTransaction *merchantTransaction = [[[self transactionData] merchantTransactions] objectAtIndex:0];
        if([[merchantTransaction payments] count] > 0) {
            
            Payment *payment = [[merchantTransaction payments] objectAtIndex:0];
            
            [payment setInstallmentCount:installmentCount];
            [payment setInterestRate:interestRate];
        }
    }
}

- (BOOL) validate {
    
    if([[self orders] count] == 0 || [[self orders] count] > 1) return NO;
    if([[[self transactionData] merchantTransactions] count] == 0 || [[[self transactionData] merchantTransactions] count] > 1) return NO;
    if([[[[[self transactionData] merchantTransactions] objectAtIndex:0] payments] count] == 0 || [[[[[self transactionData] merchantTransactions] objectAtIndex:0] payments] count] > 1) return NO;
    
    return YES;
}

@end
