//
//  OneClickCheckoutStorage.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 07/11/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "OneClickCheckoutStorage.h"
#import "StorageConstants.h"
#import "JsonHelper.h"

@implementation OneClickCheckoutStorage

+ (BOOL) getSimulationPostalCode:(NSString **) postalCode {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults) {
        
        *postalCode = [defaults stringForKey:POSTAL_CODE_KEY];
        if(postalCode) {
            return YES;
        }
    }
    
    return NO;
}

+ (BOOL) getSimulationCountry:(NSString **) simulationCountry {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults) {
        
        *simulationCountry = [defaults stringForKey:COUNTRY_KEY];
        if(simulationCountry) {
            return YES;
        }
    }
    
    return NO;
}

+ (BOOL) getSlaId:(NSString **) slaId {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults) {
        
        *slaId = [defaults stringForKey:SLA_ID_KEY];
        if(slaId) {
            return YES;
        }
    }
    
    return NO;
}

+ (BOOL) getPaymentSystemId:(NSString **) paymentSystemId {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults) {
        
        *paymentSystemId = [defaults stringForKey:PAYMENT_SYSTEM_ID_KEY];
        if(paymentSystemId) {
            return YES;
        }
    }
    
    return NO;
}

+ (BOOL) getInstallmentCount:(int *) installmentCount {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults) {
        
        *installmentCount = (int) [defaults integerForKey:INSTALLMENT_COUNT_KEY];
        return YES;
    }
    
    return NO;
}

+ (BOOL) getUserProfile:(UserProfile **) userProfile {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults) {
        
        NSDictionary *jsonDictionary = [defaults dictionaryForKey:USER_PROFILE_KEY];
        if(jsonDictionary) {
            
            NSData *jsonData;
            if([JsonHelper getJsonData:&jsonData fromDictionary:jsonDictionary]) {
                
                *userProfile = [[UserProfile alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
                if(userProfile) {
                    return YES;
                }
            }
        }
    }
    
    return NO;
}

+ (BOOL) getUserAddress:(UserAddress **) userAddress {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults) {
        
        NSDictionary *jsonDictionary = [defaults dictionaryForKey:USER_ADDRESS_KEY];
        if(jsonDictionary) {
            
            NSData *jsonData;
            if([JsonHelper getJsonData:&jsonData fromDictionary:jsonDictionary]) {
                
                *userAddress = [[UserAddress alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
                if(userAddress) {
                    return YES;
                }
            }
        }
    }
    
    return NO;
}

+ (BOOL) getCreditCard:(CreditCard **) creditCard {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults) {
        
        NSDictionary *jsonDictionary = [defaults dictionaryForKey:CREDIT_CARD_KEY];
        if(jsonDictionary) {
            
            NSData *jsonData;
            if([JsonHelper getJsonData:&jsonData fromDictionary:jsonDictionary]) {
                
                *creditCard = [[CreditCard alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
                if(creditCard) {
                    return YES;
                }
            }
        }
    }
    
    return NO;
}
 
+ (BOOL) setSimulationPostalCode:(NSString *) postalCode {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults && postalCode) {
        
        [defaults setObject:postalCode forKey:POSTAL_CODE_KEY];
        [defaults synchronize];
        
        return YES;
    }
    else return NO;
}

+ (BOOL) setSimulationCountry:(NSString *) simulationCountry {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults && simulationCountry) {
        
        [defaults setObject:simulationCountry forKey:COUNTRY_KEY];
        [defaults synchronize];
        
        return YES;
    }
    else return NO;
}

+ (BOOL) setSlaId:(NSString *) slaId {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults && slaId) {
        
        [defaults setObject:slaId forKey:SLA_ID_KEY];
        [defaults synchronize];
        
        return YES;
    }
    else return NO;
}

+ (BOOL) setPaymentSystemId:(NSString *) paymentSystemId {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults && paymentSystemId) {
        
        [defaults setObject:paymentSystemId forKey:PAYMENT_SYSTEM_ID_KEY];
        [defaults synchronize];
        
        return YES;
    }
    else return NO;
}

+ (BOOL) setInstallmentCount:(int) installmentCount {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults) {
        
        NSInteger integerInstallmentCount = (NSInteger) installmentCount;
        
        [defaults setInteger:integerInstallmentCount forKey:INSTALLMENT_COUNT_KEY];
        [defaults synchronize];
        
        return YES;
    }
    else return NO;
}

+ (BOOL) setUserProfile:(UserProfile *) userProfile {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults && userProfile) {
        
        [defaults setObject:[userProfile toJsonObject] forKey:USER_PROFILE_KEY];
        [defaults synchronize];
        
        return YES;
    }
    else return NO;
}

+ (BOOL) setUserAddress:(UserAddress *) userAddress {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults && userAddress) {
        
        [defaults setObject:[userAddress toJsonObject] forKey:USER_ADDRESS_KEY];
        [defaults synchronize];
        
        return YES;
    }
    else return NO;
}

+ (BOOL) setCreditCard:(CreditCard *) creditCard {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults && creditCard) {
        
        [defaults setObject:[creditCard toJsonObject] forKey:USER_ADDRESS_KEY];
        [defaults synchronize];
        
        return YES;
    }
    else return NO;
}

@end
