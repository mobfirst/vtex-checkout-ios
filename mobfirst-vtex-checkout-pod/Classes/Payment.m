//
//  Payment.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 29/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "Payment.h"
#import "JsonKeys.h"
#import "JsonHelper.h"

@implementation Payment

- (id) initWithPaymentSystem:(NSString *) paymentSystem value:(long) value referenceValue:(long) referenceValue installmentCount:(int) installmentCount andInterestRate:(long) interestRate {
    
    if(self = [super init]) {
        
        self.paymentSystem = paymentSystem;
        self.value = value;
        self.referenceValue = referenceValue;
        self.installmentCount = installmentCount;
        self.interestRate = interestRate;
    }
    
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        self.paymentSystem = [jsonDictionary objectForKey:_paymentSystem];
        self.value = [[jsonDictionary objectForKey:_paymentValue] longValue];
        self.referenceValue = [[jsonDictionary objectForKey:_paymentReferenceValue] longValue];
        self.installmentCount = 0;
        self.interestRate = 0;
    }
    
    return self;
}

- (void) setInstallmentCount:(int) installmentCount {
    _installmentCount = installmentCount;
}

- (void) setInterestRate:(long) interestRate {
    _interestRate = interestRate;
}

@end
