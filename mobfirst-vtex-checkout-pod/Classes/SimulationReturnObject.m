//
//  SimulationReturnObject.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 27/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "SimulationReturnObject.h"
#import "JsonKeys.h"
#import "JsonHelper.h"
#import "Item.h"
#import "LogisticsInfo.h"
#import "Message.h"
#import "PaymentData.h"
#import "Sla.h"
#import "InstallmentOption.h"
#import "Installment.h"

@implementation SimulationReturnObject

- (id) initWithItems:(NSMutableArray *) items paymentData:(PaymentData *) paymentData logisticsInfo:(NSMutableArray *) logisticsInfo messages:(NSMutableArray *) messages selectedSlaId:(NSString *) selectedSlaId selectedInstallmentCount:(int) selectedInstallmentCount andSelectedPaymentSystem:(NSString *) selectedPaymentSystem {
    
    if(self = [super init]) {
        
        self.items = items;
        self.paymentData = paymentData;
        self.logisticsInfo = logisticsInfo;
        self.messages = messages;
        self.selectedSlaId = selectedSlaId;
        self.selectedInstallmentCount = selectedInstallmentCount;
        self.selectedPaymentSystem = selectedPaymentSystem;
    }
    
    return self;
}
- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        NSData *jsonData;
        NSArray *jsonArray;
        
        self.items = [[NSMutableArray alloc] init];
        self.logisticsInfo = [[NSMutableArray alloc] init];
        self.messages = [[NSMutableArray alloc] init];
        
        if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        if(![JsonHelper getJsonData:&jsonData fromDictionary:jsonDictionary withJsonKey:_simulationReturnObjectItems]) return self;
        if(![JsonHelper getJsonArray:&jsonArray fromJsonData:jsonData]) return self;
        
        for(NSDictionary *item in jsonArray) {
            
            NSData *jsonData;
            if(![JsonHelper getJsonData:&jsonData fromDictionary:item]) continue;
            
            [self.items addObject:[[Item alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]]];
        }
        
        if(![JsonHelper getJsonData:&jsonData fromDictionary:jsonDictionary withJsonKey:_simulationReturnObjectLogisticsInfo]) return self;
        if(![JsonHelper getJsonArray:&jsonArray fromJsonData:jsonData]) return self;
        
        for(NSDictionary *item in jsonArray) {
            
            NSData *jsonData;
            if(![JsonHelper getJsonData:&jsonData fromDictionary:item]) continue;
            
            [self.logisticsInfo addObject:[[LogisticsInfo alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]]];
        }
        
        if(![JsonHelper getJsonData:&jsonData fromDictionary:jsonDictionary withJsonKey:_simulationReturnObjectMessages]) return self;
        if(![JsonHelper getJsonArray:&jsonArray fromJsonData:jsonData]) return self;
        
        for(NSDictionary *item in jsonArray) {
            
            NSData *jsonData;
            if(![JsonHelper getJsonData:&jsonData fromDictionary:item]) continue;
            
            [self.messages addObject:[[Message alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]]];
        }
        
        if(![JsonHelper getJsonData:&jsonData fromDictionary:jsonDictionary withJsonKey:_simulationReturnObjectPaymentData]) return self;        
        self.paymentData = [[PaymentData alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
    }
    
    return self;
}

- (SimulationReturnObject *) selectSla:(NSString *) slaId forItemIndex:(int) itemIndex {
    
    NSMutableArray *filteredLogisticsInfo = [[NSMutableArray alloc] init];
    if(slaId) {
        
        for(LogisticsInfo *logisticsInfo in [self logisticsInfo]) {
            if([logisticsInfo itemIndex] == itemIndex) {
                
                NSArray *slas = [logisticsInfo slas];
                for(Sla *sla in slas) {
                    
                    if([slaId isEqualToString:[sla id]]) {
                        
                        LogisticsInfo *newLogisticsInfo = [[LogisticsInfo alloc] initWithItemIndex:[logisticsInfo itemIndex] stockBalance:[logisticsInfo stockBalance] quantity:[logisticsInfo quantity] slas:[[NSMutableArray alloc] initWithObjects:sla, nil]];
                        
                        [newLogisticsInfo selectSlaWithName:[sla name] lockTTL:[sla lockTTL] shippingEstimate:[sla shippingEstimate] andPrice:[sla price]];
                        [filteredLogisticsInfo addObject:newLogisticsInfo];
                        
                        self.selectedSlaId = slaId;
                        break;
                    }
                }
            }
            else [filteredLogisticsInfo addObject:logisticsInfo];
        }
        
        return [[SimulationReturnObject alloc] initWithItems:[self items] paymentData:[self paymentData] logisticsInfo:filteredLogisticsInfo messages:[self messages] selectedSlaId:[self selectedSlaId] selectedInstallmentCount:[self selectedInstallmentCount] andSelectedPaymentSystem:[self selectedPaymentSystem]];
    }
    
    return nil;
}

- (SimulationReturnObject *) selectInstallmentsWithPaymentSystem:(NSString *) paymentSystem count:(int) count {
    
    NSMutableArray *filteredInstallmentOptions = [[NSMutableArray alloc] init];
    NSArray *installmentOptions = [[self paymentData] installmentOptions];
    
    if(paymentSystem) {
        
        for(InstallmentOption *installmentOption in installmentOptions) {
            if([paymentSystem isEqualToString:[installmentOption paymentSystem]]) {
                
                NSArray *installments = [installmentOption installments];
                for(Installment *installment in installments) {
                    
                    if([installment count] == count) {
                        
                        InstallmentOption *newInstallmentOption = [[InstallmentOption alloc] initWithPaymentSystem:[installmentOption paymentSystem] bin:[installmentOption bin] paymentName:[installmentOption paymentName] paymentGroupName:[installmentOption paymentGroupName] value:[installmentOption value] installments:[[NSMutableArray alloc] initWithObjects:installment, nil]];
                        
                        [newInstallmentOption selectInstallmentCount:count];
                        [filteredInstallmentOptions addObject:newInstallmentOption];
                        
                        self.selectedInstallmentCount = count;
                        self.selectedPaymentSystem = paymentSystem;
                        
                        break;
                    }
                }
                
                break;
            }
        }
        
        return [[SimulationReturnObject alloc] initWithItems:[self items] paymentData:[[PaymentData alloc] initWithInstallmentOptions:filteredInstallmentOptions] logisticsInfo:[self logisticsInfo] messages:[self messages] selectedSlaId:[self selectedSlaId] selectedInstallmentCount:[self selectedInstallmentCount] andSelectedPaymentSystem:[self selectedPaymentSystem]];
    }
    
    return nil;
}

- (BOOL) validate {
    
    for(LogisticsInfo *logisticsInfo in [self logisticsInfo]) {
        if(![logisticsInfo isSlaSelected]) return NO;
    }
    
    if([[[self paymentData] installmentOptions] count] == 0 || [[[self paymentData] installmentOptions] count] > 1) return NO;
    if(![[[[self paymentData] installmentOptions] objectAtIndex:0] isInstallmentCountSelected]) return NO;
    
    return YES;
}

@end
