//
//  MasterDataApi.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 28/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserProfile.h"
#import "UserAddress.h"
#import "UserAddresses.h"
#import "ErrorType.h"

@interface MasterDataApi : NSObject

@property (nonatomic, strong) NSDictionary *headers;
@property (nonatomic, strong) NSString *accountName;
@property (nonatomic, strong) NSString *vtexApiKey;
@property (nonatomic, strong) NSString *vtexApiToken;

- (id) initWithAccountName:(NSString *) acoountName vtexApiKey:(NSString *) vtexApiKey andVtexApiToken:(NSString *) vtexApiToken;

- (void) getUserProfileWithUserId:(NSString *) userId andCompletionHandler:(void (^)(UserProfile *userProfile, ErrorType error)) completionHandler;
- (void) updateUserProfile:(UserProfile *) oldUserProfile withNewUserProfile:(UserProfile *) newUserProfile andCompletionHandler:(void (^)(ErrorType error)) completionHandler;
- (void) getUserAddressesFromUserProfile:(UserProfile *) userProfile andCompletionHandler:(void (^)(UserAddresses *addresses, ErrorType error)) completionHandler;
- (void) updateUserAddress:(UserAddress *) oldUserAddress withNewUserAddress:(UserAddress *) newUserAddress andCompletionHandler:(void (^)(ErrorType error)) completionHandler;
- (void) createUserAddressWithUserProfile:(UserProfile *) userProfile userAddress:(UserAddress *) userAddress andCompletionHandler:(void (^)(ErrorType error)) completionHandler;

@end
