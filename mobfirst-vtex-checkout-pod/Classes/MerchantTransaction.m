//
//  MerchantTransaction.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 29/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "MerchantTransaction.h"
#import "JsonKeys.h"
#import "JsonHelper.h"
#import "Payment.h"

@implementation MerchantTransaction

- (id) initWithTransactionId:(NSString *)transactionId merchantName:(NSString *) merchantName andPayments:(NSMutableArray *) payments {
    
    if(self = [super init]) {
        
        self.transactionId = transactionId;
        self.merchantName = merchantName;
        self.payments = payments;
    }
    
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        NSData *jsonData;
        NSArray *jsonArray;
        
        self.payments = [[NSMutableArray alloc] init];
        
        if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        if(![JsonHelper getJsonData:&jsonData fromDictionary:jsonDictionary withJsonKey:_merchantTransactionPayments]) return self;
        if(![JsonHelper getJsonArray:&jsonArray fromJsonData:jsonData]) return self;
        
        for(NSDictionary *item in jsonArray) {
            
            NSData *jsonData;
            if(![JsonHelper getJsonData:&jsonData fromDictionary:item]) continue;
            
            [self.payments addObject:[[Payment alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]]];
        }
        
        self.transactionId = [jsonDictionary objectForKey:_merchantTransactionTransactionId];
        self.merchantName = [jsonDictionary objectForKey:_merchantTransactionMerchantName];
    }
    
    return self;
}

- (NSDictionary *) toJsonObject {
    
    return [[NSDictionary alloc] initWithObjectsAndKeys:
            [self transactionId], _merchantTransactionId,
            [self merchantName], _merchantTransactionMerchantName, nil];
}

@end
