//
//  SimulationReturnObject.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 27/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaymentData.h"

@interface SimulationReturnObject : NSObject

@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) PaymentData *paymentData;
@property (nonatomic, strong) NSMutableArray *logisticsInfo;
@property (nonatomic, strong) NSMutableArray *messages;

@property (nonatomic, strong) NSString *selectedSlaId;
@property (nonatomic) int selectedInstallmentCount;
@property (nonatomic, strong) NSString *selectedPaymentSystem;

- (id) initWithItems:(NSMutableArray *) items paymentData:(PaymentData *) paymentData logisticsInfo:(NSMutableArray *) logisticsInfo messages:(NSMutableArray *) messages selectedSlaId:(NSString *) selectedSlaId selectedInstallmentCount:(int) selectedInstallmentCount andSelectedPaymentSystem:(NSString *) selectedPaymentSystem;
- (id) initWithJsonString:(NSString *) jsonString;

- (SimulationReturnObject *) selectSla:(NSString *) slaId forItemIndex:(int) itemIndex;
- (SimulationReturnObject *) selectInstallmentsWithPaymentSystem:(NSString *) paymentSystem count:(int) count;
- (BOOL) validate;

@end
