//
//  Sla.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 28/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "Sla.h"
#import "JsonKeys.h"
#import "JsonHelper.h"

@implementation Sla

- (id) initWithId:(NSString *) id name:(NSString *) name shippingEstimate:(NSString *) shippingEstimate lockTTL:(NSString *) lockTTL price:(long) price listPrice:(long) listPrice andTax:(long) tax {
    
    if(self = [super init]) {
        
        self.id = id;
        self.name = name;
        self.shippingEstimate = shippingEstimate;
        self.lockTTL = lockTTL;
        self.price = price;
        self.listPrice = listPrice;
        self.tax = tax;
    }
    
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        self.id = [jsonDictionary objectForKey:_slaId];
        self.name = [jsonDictionary objectForKey:_slaName];
        self.shippingEstimate = [jsonDictionary objectForKey:_slaShippingEstimate];
        self.lockTTL = [jsonDictionary objectForKey:_slaLockTTL];
        self.price = [[jsonDictionary objectForKey:_slaPrice] longValue];
        self.listPrice = [[jsonDictionary objectForKey:_slaListPrice] longValue];
        self.tax = [[jsonDictionary objectForKey:_slaTax] longValue];
    }
    
    return self;
}

@end
