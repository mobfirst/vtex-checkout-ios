//
//  Order.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 29/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Order : NSObject

@property (nonatomic, strong) NSString *orderId;
@property (nonatomic, strong) NSString *orderGroup;

- (id) initWithOrderId:(NSString *) orderId andOrderGroup:(NSString *) orderGroup;
- (id) initWithJsonString:(NSString *) jsonString;

@end
