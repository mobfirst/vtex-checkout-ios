//
//  TransactionData.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 29/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "TransactionData.h"
#import "JsonKeys.h"
#import "JsonHelper.h"
#import "MerchantTransaction.h"

@implementation TransactionData

- (id) initWithMerchantTransactions:(NSMutableArray *) merchantTransactions {
    
    if(self = [super init]) self.merchantTransactions = merchantTransactions;
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        NSData *jsonData;
        NSArray *jsonArray;
        
        self.merchantTransactions = [[NSMutableArray alloc] init];
        
        if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        if(![JsonHelper getJsonData:&jsonData fromDictionary:jsonDictionary withJsonKey:_transactionDataMerchantTransactions]) return self;
        if(![JsonHelper getJsonArray:&jsonArray fromJsonData:jsonData]) return self;
        
        for(NSDictionary *item in jsonArray) {
            
            NSData *jsonData;
            if(![JsonHelper getJsonData:&jsonData fromDictionary:item]) continue;
            
            [self.merchantTransactions addObject:[[MerchantTransaction alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]]];
        }
    }
    
    return self;
}

@end
