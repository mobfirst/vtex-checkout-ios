//
//  SellerMerchantInstallment.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 28/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SellerMerchantInstallment : NSObject

@property (nonatomic, strong) NSString *id;
@property (nonatomic) int count;
@property (nonatomic) BOOL hasInterestRate;
@property (nonatomic) long interestRate;
@property (nonatomic) long value;
@property (nonatomic) long total;

- (id) initWithId:(NSString *) id count:(int) count hasInterestRate:(BOOL) hasInterestRate interestRate:(long) interestRate value:(long) value andTotal:(long) total;
- (id) initWithJsonString:(NSString *) jsonString;

@end
