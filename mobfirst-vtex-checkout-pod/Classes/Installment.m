//
//  Installment.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 28/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "Installment.h"
#import "JsonKeys.h"
#import "JsonHelper.h"
#import "SellerMerchantInstallment.h"

@implementation Installment

- (id) initWithCount:(int) count hasInterestRate:(BOOL) hasInterestRate interestRate:(long) interestRate value:(long) value total:(long) total andSellerMerchantInstallments:(NSMutableArray *) sellerMerchantInstallments {
    
    if(self = [super init]) {
        
        self.count = count;
        self.hasInterestRate = hasInterestRate;
        self.interestRate = interestRate;
        self.value = value;
        self.total = total;
        self.sellerMerchantInstallments = sellerMerchantInstallments;
    }
    
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        NSData *jsonData;
        NSArray *jsonArray;
        
        if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        if(![JsonHelper getJsonData:&jsonData fromDictionary:jsonDictionary withJsonKey:_installmentSellerMerchantInstallments]) return self;
        if(![JsonHelper getJsonArray:&jsonArray fromJsonData:jsonData]) return self;
        
        for(NSDictionary *item in jsonArray) {
            
            NSData *jsonData;
            if(![JsonHelper getJsonData:&jsonData fromDictionary:item]) continue;
            
            [self.sellerMerchantInstallments addObject:[[SellerMerchantInstallment alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]]];
        }
        
        self.count = [[jsonDictionary objectForKey:_sellerMerchantInstallmentCount] intValue];
        self.hasInterestRate = [[jsonDictionary objectForKey:_sellerMerchantInstallmentHasInterestRate] boolValue];
        self.interestRate = [[jsonDictionary objectForKey:_sellerMerchantInstallmentInterestRate] longValue];
        self.value = [[jsonDictionary objectForKey:_sellerMerchantInstallmentValue] longValue];
        self.total = [[jsonDictionary objectForKey:_sellerMerchantInstallmentTotal] longValue];
    }
    
    return self;
}

@end
