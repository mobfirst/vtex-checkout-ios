//
//  Error.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 28/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "Error.h"
#import "JsonKeys.h"
#import "JsonHelper.h"

@implementation Error

- (id) initWithCode:(NSString *) code message:(NSString *) message andExsception:(NSString *) exception {
    
    if(self = [super init]) {
        
        self.code = code;
        self.message = message;
        self.exception = exception;
    }
    
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        self.code = [jsonDictionary objectForKey:_errorCode];
        self.message = [jsonDictionary objectForKey:_errorMessage];
        self.exception = [jsonDictionary objectForKey:_errorException];
    }
    
    return self;
}

@end
