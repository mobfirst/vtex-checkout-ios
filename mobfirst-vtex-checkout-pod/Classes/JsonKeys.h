//
//  JsonKeys.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 28/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#ifndef JsonKeys_h
#define JsonKeys_h

// SellerMerchantInstallment keys
#define _sellerMerchantInstallmentId @"id"
#define _sellerMerchantInstallmentCount @"count"
#define _sellerMerchantInstallmentHasInterestRate @"hasInterestRate"
#define _sellerMerchantInstallmentInterestRate @"interestRate"
#define _sellerMerchantInstallmentValue @"value"
#define _sellerMerchantInstallmentTotal @"total"

// Installment keys
#define _installmentInstallmentCount @"count"
#define _installmentHasInterestRate @"hasInterestRate"
#define _installmentInterestRate @"interestRate"
#define _installmentValue @"value"
#define _installmentReferenceValue @"referenceValue"
#define _installmentTotal @"total"
#define _installmentSellerMerchantInstallments @"sellerMerchantInstallments"
#define _installmentSelectedInstallmentCount @"installments"

// InstallmentOption keys
#define _installmentOptionPaymentSystem @"paymentSystem"
#define _installmentOptionBin @"bin"
#define _installmentOptionPaymentName @"paymentName"
#define _installmentOptionPaymentGroupName @"paymentGroupName"
#define _installmentOptionValue @"value"
#define _installmentOptionInstallments @"installments"

// PaymentData keys
#define _paymentDataInstallmentOptions @"installmentOptions"

// Sla keys
#define _slaId @"id"
#define _slaName @"name"
#define _slaShippingEstimate @"shippingEstimate"
#define _slaLockTTL @"lockTTL"
#define _slaPrice @"price"
#define _slaListPrice @"listPrice"
#define _slaTax @"tax"

// LogisticsInfo keys
#define _logisticsInfoItemIndex @"itemIndex"
#define _logisticsInfoStockBalance @"stockBalance"
#define _logisticsInfoQuantity @"quantity"
#define _logisticsInfoSlas @"slas"
#define _logisticsInfoSelectedSla @"selectedSla"

// Item keys
#define _itemId @"id"
#define _itemQuantity @"quantity"
#define _itemSeller @"seller"
#define _itemPrice @"price"

// Simulation keys
#define _simulationItems @"items"
#define _simulationPostalCode @"postalCode"
#define _simulationCountry @"country"

// Simulation response keys
#define _simulationReturnObjectItems @"items"
#define _simulationReturnObjectPaymentData @"paymentData"
#define _simulationReturnObjectLogisticsInfo @"logisticsInfo"
#define _simulationReturnObjectMessages @"messages"

// Message keys
#define _messageCode @"code"
#define _messageText @"text"
#define _messageStatus @"status"

// Address keys
#define _addressId @"id"
#define _addressType @"addressType"
#define _addressReceiverName @"receiverName"
#define _addressAddressId @"addressId"
#define _addressPostalCode @"postalCode"
#define _addressCity @"city"
#define _addressState @"state"
#define _addressCountry @"country"
#define _addressStreet @"street"
#define _addressNumber @"number"
#define _addressNeighborhood @"neighborhood"
#define _addressComplement @"complement"
#define _addressReference @"reference"
#define _addressUserId @"userId"

// User profile keys
#define _profileId @"id"
#define _profileEmail @"email"
#define _profileFirstName @"firstName"
#define _profileLastName @"lastName"
#define _profileDocument @"document"
#define _profileDocumentType @"documentType"
#define _profilePhone @"phone"

// Place order keys
#define _placeOrderItems @"items"
#define _placeOrderClientProfileData @"clientProfileData"
#define _placeOrderShippingData @"shippingData"
#define _placeOrderPaymentData @"paymentData"

// Shipping data keys
#define _shippingDataId @"id"
#define _shippingDataAddress @"address"
#define _shippingDataLogisticsInfo @"logisticsInfo"

// Payment data keys
#define _paymentDataId @"id"
#define _paymentDataPayments @"payments"

// Payment API option keys
#define _paymentApiOptionQuantity @"quantity"
#define _paymentApiOptionValue @"value"
#define _paymentApiOptionInterestRate @"interestRate"

// Payment API installment keys
#define _paymentApiInstallmentOptions @"options"

// Payment API installments return object keys
#define _paymentApiInstallmentsReturnObjectValue @"value"
#define _paymentApiInstallmentsReturnObjectInstallments @"installments"

// Error keys
#define _errorCode @"code"
#define _errorMessage @"message"
#define _errorException @"exception"

// Error return object keys
#define _errorReturnObjectError @"error"

// Payment keys
#define _paymentSystem @"paymentSystem"
#define _paymentValue @"value"
#define _paymentReferenceValue @"referenceValue"

// Merchant transaction keys
#define _merchantTransactionId @"id"
#define _merchantTransactionTransactionId @"transactionId"
#define _merchantTransactionMerchantName @"merchantName"
#define _merchantTransactionPayments @"payments"

// Transaction data keys
#define _transactionDataMerchantTransactions @"merchantTransactions"

// Place order return object keys
#define _placeOrderReturnObjectTransactionData @"transactionData"
#define _placeOrderReturnObjectOrders @"orders"

// Order keys
#define _orderId @"orderId"
#define _orderGroup @"orderGroup"

// Credit card keys
#define _creditCardHolderName @"holderName"
#define _creditCardNumber @"cardNumber"
#define _creditCardValidationCode @"validationCode"
#define _creditCardDueDate @"dueDate"
#define _creditCardDocument @"document"
#define _creditCardAccountId @"accountId"
#define _creditCardAddress @"address"
#define _creditCardCallbackUrl @"callbackUrl"

// Send payment keys
#define _sendPaymentSystem @"paymentSystem"
#define _sendPaymentInstallments @"installments"
#define _sendPaymentValue @"value"
#define _sendPaymentInstallmentsInterestRate @"installmentsInterestRate"
#define _sendPaymentInstallmentsValue @"installmentsValue"
#define _sendPaymentReferenceValue @"referenceValue"
#define _sendPaymentFields @"fields"
#define _sendPaymentTransaction @"transaction"

// Send payment keys
#define _simulationReturnObjectItems @"items"
#define _simulationReturnObjectPaymentData @"paymentData"
#define _simulationReturnObjectLogisticsInfo @"logisticsInfo"
#define _simulationReturnObjectMessages @"messages"

// Payment method keys
#define _paymentMethodId @"id"
#define _paymentMethodName @"name"
#define _paymentMethodIsAvailable @"isAvailable"

#endif /* JsonKeys_h */
