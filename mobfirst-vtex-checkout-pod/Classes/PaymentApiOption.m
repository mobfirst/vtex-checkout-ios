//
//  PaymentApiOption.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 30/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "PaymentApiOption.h"
#import "JsonKeys.h"
#import "JsonHelper.h"

@implementation PaymentApiOption

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        self.quantity = [[jsonDictionary objectForKey:_paymentApiOptionQuantity] intValue];
        self.value = [[jsonDictionary objectForKey:_paymentApiOptionValue] longValue];
        self.interestRate = [[jsonDictionary objectForKey:_paymentApiOptionInterestRate] longValue];
    }
    
    return self;
}
@end
