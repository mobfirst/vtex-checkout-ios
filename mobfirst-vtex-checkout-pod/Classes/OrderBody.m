//
//  OrderBody.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 30/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "OrderBody.h"
#import "Item.h"
#import "InstallmentOption.h"
#import "JsonKeys.h"
#import "JsonValues.h"
#import "LogisticsInfo.h"

@implementation OrderBody

- (id) initWithSimulationReturnObject:(SimulationReturnObject *) simulationReturnObject valueWithInterest:(long) valueWithInterest referenceValue:(long) referenceValue userProfile:(UserProfile *) userProfile andUserAddress:(UserAddress *) userAddress {
    
    if(self = [super init]) {
        
        self.simulationReturnObject = simulationReturnObject;
        self.valueWithInterest = valueWithInterest;
        self.referenceValue = referenceValue;
        self.userProfile = userProfile;
        self.userAddress = userAddress;
    }
    
    return self;
}

- (NSDictionary *) toJsonObject {
    
    NSMutableArray *paymentsArray = [[NSMutableArray alloc] init];
    NSMutableArray *logisticsInfoArray = [[NSMutableArray alloc] init];
    NSMutableArray *itemsArray = [[NSMutableArray alloc] init];
    
    if([[[[self simulationReturnObject] paymentData] installmentOptions] count] == 1) {
        
        InstallmentOption *installmentOption = [[[[self simulationReturnObject] paymentData]installmentOptions] objectAtIndex:0];
        
        [installmentOption setValueWithInterest:[self valueWithInterest]];
        [installmentOption setReferenceValue:[self referenceValue]];
        
        [paymentsArray addObject:[installmentOption toJsonObject]];
    }
    
    for(LogisticsInfo *logisticsInfo in [[self simulationReturnObject] logisticsInfo]) [logisticsInfoArray addObject:[logisticsInfo toJsonObject]];
    for(Item *item in [[self simulationReturnObject] items]) [itemsArray addObject:[item toJsonObject]];
    
    NSDictionary *shippingDataObject = [[NSDictionary alloc] initWithObjectsAndKeys:
                                        _paymentDataIdValue, _paymentDataId,
                                        [[self userAddress] toJsonObject], _shippingDataAddress,
                                        logisticsInfoArray, _shippingDataLogisticsInfo, nil];
    
    NSDictionary *paymentDataObject = [[NSDictionary alloc] initWithObjectsAndKeys:
                                       _paymentDataIdValue, _paymentDataId,
                                       paymentsArray, _paymentDataPayments, nil];
    
    return [[NSDictionary alloc] initWithObjectsAndKeys:
            itemsArray, _placeOrderItems,
            [[self userProfile] toJsonObject], _placeOrderClientProfileData,
            shippingDataObject, _placeOrderShippingData,
            paymentDataObject, _placeOrderPaymentData, nil];
}

@end
