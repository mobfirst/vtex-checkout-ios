//
//  PlaceOrderReturnObject.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 29/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TransactionData.h"

@interface PlaceOrderReturnObject : NSObject

@property (nonatomic, strong) TransactionData *transactionData;
@property (nonatomic, strong) NSMutableArray *orders;

- (id) initWithTransactionData:(TransactionData *) transactionData andOrders:(NSMutableArray *) orders;
- (id) initWithJsonString:(NSString *) jsonString;

- (void) setInstallmentData:(int) installmentCount interestRate:(long) interestRate;
- (BOOL) validate;
@end
