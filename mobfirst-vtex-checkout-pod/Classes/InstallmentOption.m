//
//  InstallmentOption.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 28/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "InstallmentOption.h"
#import "JsonKeys.h"
#import "JsonHelper.h"
#import "Installment.h"

@implementation InstallmentOption

- (id) initWithPaymentSystem:(NSString *) paymentSystem bin:(NSString *) bin paymentName:(NSString *) paymentName paymentGroupName:(NSString *) paymentGroupName value:(long) value installments:(NSMutableArray *) installments {
    
    if(self = [super init]) {
        
        self.paymentSystem = paymentSystem;
        self.bin = bin;
        self.paymentName = paymentName;
        self.paymentGroupName = paymentGroupName;
        self.value = value;
        self.installments = installments;
        self.selectedInstallmentCount = 0;
        self.selectedInterestRate = 0;
        self.valueWithInterest = 0;
        self.referenceValue = 0;
        self.isInstallmentCountSelected = NO;
    }
    
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        NSData *jsonData;
        NSArray *jsonArray;
        
        self.installments = [[NSMutableArray alloc] init];
        
        if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        if(![JsonHelper getJsonData:&jsonData fromDictionary:jsonDictionary withJsonKey:_installmentOptionInstallments]) return self;
        if(![JsonHelper getJsonArray:&jsonArray fromJsonData:jsonData]) return self;
        
        for(NSDictionary *item in jsonArray) {
            
            NSData *jsonData;
            if(![JsonHelper getJsonData:&jsonData fromDictionary:item]) continue;
            
            [self.installments addObject:[[Installment alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]]];
        }
        
        self.paymentSystem = [jsonDictionary objectForKey:_installmentOptionPaymentSystem];
        self.bin = [jsonDictionary objectForKey:_installmentOptionBin];
        self.paymentName = [jsonDictionary objectForKey:_installmentOptionPaymentName];
        self.paymentGroupName = [jsonDictionary objectForKey:_installmentOptionPaymentGroupName];
        self.value = [[jsonDictionary objectForKey:_installmentOptionValue] longValue];
        self.selectedInstallmentCount = 0;
        self.selectedInterestRate = 0;
        self.valueWithInterest = 0;
        self.referenceValue = 0;
        self.isInstallmentCountSelected = NO;
    }
    
    return self;
}

- (void) selectInstallmentCount:(int) count {
    
    for(Installment *installment in self.installments) {
        if([installment count] == count) {
            
            self.selectedInterestRate = [installment interestRate];
            
            self.selectedInstallmentCount = count;
            self.isInstallmentCountSelected = YES;
            
            break;
        }
    }
}

- (void) setValueWithInterest:(long) valueWithInterest {
    _valueWithInterest = valueWithInterest;
}

- (void) setReferenceValue:(long) referenceValue {
    _referenceValue = referenceValue;
}

- (NSDictionary *) toJsonObject {
    
    return [[NSDictionary alloc] initWithObjectsAndKeys:
            [self paymentSystem], _installmentOptionPaymentSystem,
            [NSNumber numberWithLong:[self referenceValue]], _installmentReferenceValue,
            [NSNumber numberWithLong:[self valueWithInterest]], _installmentValue,
            [NSNumber numberWithInt:[self selectedInstallmentCount]], _installmentSelectedInstallmentCount, nil];
}

@end
