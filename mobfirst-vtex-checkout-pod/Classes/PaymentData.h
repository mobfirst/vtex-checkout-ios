//
//  PaymentData.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 29/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaymentData : NSObject

@property (nonatomic, strong) NSMutableArray *installmentOptions;

- (id) initWithInstallmentOptions:(NSMutableArray *) installmentOptions;
- (id) initWithJsonString:(NSString *) jsonString;

@end
