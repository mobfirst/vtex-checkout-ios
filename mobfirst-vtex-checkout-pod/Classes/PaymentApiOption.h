//
//  PaymentApiOption.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 30/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaymentApiOption : NSObject

@property (nonatomic) int quantity;
@property (nonatomic) long value;
@property (nonatomic) long interestRate;

- (id) initWithJsonString:(NSString *) jsonString;

@end
