//
//  ErrorReturnObject.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 28/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Error.h"

@interface ErrorReturnObject : NSObject

@property (nonatomic, strong) Error *error;

- (id) initWithError:(Error *) error;
- (id) initWithJsonString:(NSString *) jsonString;

@end
