//
//  PaymentApi.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 28/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "PaymentApi.h"
#import "ApiHeaders.h"
#import "InstallmentOption.h"
#import "LogisticsInfo.h"
#import "ApiParameters.h"
#import "ApiUrls.h"
#import "HttpCheckoutConnection.h"
#import "SendPaymentBody.h"
#import "JsonHelper.h"
#import "MerchantTransaction.h"
#import "CheckoutApi.h"
#import "OneClickCheckoutStorage.h"

@implementation PaymentApi

- (id) initWithAccountName:(NSString *) accountName vtexApiKey:(NSString *) vtexApiKey andVtexApiToken:(NSString *) vtexApiToken {
    
    if(self = [super init]) {
        
        self.headers = [[NSDictionary alloc] initWithObjectsAndKeys:
                        _acceptHeaderValue, _acceptHeaderKey,
                        _contentTypeHeaderValue, _contentTypeHeaderKey,
                        vtexApiKey, _apiKeyHeaderKey,
                        vtexApiToken, _apiTokenHeaderKey, nil];
        
        self.accountName = accountName;
        self.vtexApiKey = vtexApiKey;
        self.vtexApiToken = vtexApiToken;
    }
    
    return self;
}

- (void) getPaymentMethodsWithCompletionHandler:(void (^)(PaymentMethodsReturnObject *paymentMethodsReturnObject, ErrorType error)) completionHandler {
    
    [HttpCheckoutConnection doGetFromUrl:[NSString stringWithFormat:_paymentSystemsUrl, [self accountName]] withHeaders:[self headers] parameters:nil andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!error && response) {
            if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                
                PaymentMethodsReturnObject *paymentMethodsReturnObject = [[PaymentMethodsReturnObject alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                
                if(paymentMethodsReturnObject) completionHandler(paymentMethodsReturnObject, NONE);
                else completionHandler(nil, RESPONSE_ERROR);
            }
            else completionHandler(nil, RESPONSE_ERROR);
        }
        else completionHandler(nil, COMMUNICATION_ERROR);
    }];
}

- (void) getInstallmentsWithPaymentSystem:(NSString *) paymentSystem value:(long) value andCompletionHandler:(void (^)(PaymentApiInstallmentsReturnObject *paymentApiInstallmentsReturnObject, ErrorType error)) completionHandler {
    
    if(paymentSystem) {
        
        NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                    [NSString stringWithFormat:@"%ld", value], _installmentsValueParameterKey,
                                    paymentSystem, _installmentsPaymentMethodIdParameterKey,
                                    [NSString stringWithFormat:@"%ld", value], _installmentsPaymentMethodValueParameterKey, nil];
        
        [HttpCheckoutConnection doGetFromUrl:[NSString stringWithFormat:_installmentsUrl, [self accountName]] withHeaders:[self headers] parameters:parameters andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(!error && response) {
                if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                    
                    PaymentApiInstallmentsReturnObject *paymentApiInstallmentsReturnObject = [[PaymentApiInstallmentsReturnObject alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                    
                    if(paymentApiInstallmentsReturnObject) completionHandler(paymentApiInstallmentsReturnObject, NONE);
                    else completionHandler(nil, RESPONSE_ERROR);
                }
                else completionHandler(nil, RESPONSE_ERROR);
            }
            else completionHandler(nil, COMMUNICATION_ERROR);
        }];
    }
    else completionHandler(nil, PARAM_ERROR);
}

- (void) getValueWithInterestFromSimulationReturnObject:(SimulationReturnObject *) simulationReturnObject withCompletionHandler:(void (^)(long valueWithInterest, long referenceValue, ErrorType error)) completionHandler {
    
    if(simulationReturnObject) {
        if([simulationReturnObject validate]) {
            
            __block InstallmentOption *installmentOption = [[[simulationReturnObject paymentData] installmentOptions] objectAtIndex:0];
            __block long totalWithoutInterest = [installmentOption value];
            
            for(LogisticsInfo *logisticsInfo in [simulationReturnObject logisticsInfo]) totalWithoutInterest += [logisticsInfo price];
            
            [self getInstallmentsWithPaymentSystem:[installmentOption paymentSystem] value:totalWithoutInterest andCompletionHandler:^(PaymentApiInstallmentsReturnObject *paymentApiInstallmentsReturnObject, ErrorType error) {
                
                if(paymentApiInstallmentsReturnObject && error == NONE) {
                    
                    long valueWithInterest;
                    if([paymentApiInstallmentsReturnObject valueWithInterest:&valueWithInterest forInstallmentCount:[installmentOption selectedInstallmentCount]]) completionHandler(valueWithInterest, totalWithoutInterest, NONE);
                    else completionHandler(0, 0, PARAM_VALIDATION_ERROR);
                }
                else completionHandler(0, 0, RESPONSE_ERROR);
            }];
        }
        else completionHandler(0, 0, PARAM_VALIDATION_ERROR);
    }
    else completionHandler(0, 0, PARAM_ERROR);
}

- (void) sentPaymentWithPlaceOrderReturnObject:(PlaceOrderReturnObject *) placeOrderReturnObject creditCard:(CreditCard *) creditCard andCompletionHandler:(void (^)(ErrorType error)) completionHandler {
    
    if(placeOrderReturnObject) {
        if([placeOrderReturnObject validate]) {
            
            NSData *bodyData;
            __block CreditCard *card = creditCard;
            
            NSArray *bodyArray = [[[SendPaymentBody alloc] initWithPlaceOrderReturnObject:placeOrderReturnObject andCreditCard:creditCard] toJsonObject];
            if([JsonHelper getJsonData:&bodyData fromArray:bodyArray]) {
                
                [HttpCheckoutConnection doPostToUrl:[NSString stringWithFormat:_sendPaymentUrl, [self accountName], [[[[placeOrderReturnObject transactionData] merchantTransactions] objectAtIndex:0] transactionId]] withHeaders:[self headers] parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    
                    if(!error && response) {
                        if([((NSHTTPURLResponse *) response) statusCode] == 201) {
                            
                            CheckoutApi *checkoutApi = [[CheckoutApi alloc] initWithAccountName:[self accountName] vtexApiKey:[self vtexApiKey] andVtexApiToken:[self vtexApiToken]];
                            [checkoutApi gatewayCallbackWithPlaceOrderReturnObject:placeOrderReturnObject andCompletionHandler:^(ErrorType error) {
                                
                                if(error == NONE) {
                                    
                                    if(card) [OneClickCheckoutStorage setCreditCard:card];
                                    completionHandler(NONE);
                                }
                                else completionHandler(error);
                            }];
                        }
                        else completionHandler(RESPONSE_ERROR);
                    }
                    else completionHandler(COMMUNICATION_ERROR);
                }];
            }
            else completionHandler(JSON_ERROR);
        }
        else completionHandler(PARAM_VALIDATION_ERROR);
    }
    else completionHandler(PARAM_ERROR);
}

@end
