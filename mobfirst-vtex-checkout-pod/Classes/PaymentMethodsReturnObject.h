//
//  PaymentMethodsReturnObject.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 06/11/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaymentMethodsReturnObject : NSObject

@property (nonatomic, strong) NSMutableArray *paymentMethods;

- (id) initWithJsonString:(NSString *) jsonString;

@end
