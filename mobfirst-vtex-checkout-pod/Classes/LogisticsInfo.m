//
//  LogisticsInfo.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 28/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "LogisticsInfo.h"
#import "JsonKeys.h"
#import "JsonHelper.h"
#import "Sla.h"

@implementation LogisticsInfo

- (id) initWithItemIndex:(int) itemIndex stockBalance:(int) stockBalance quantity:(int) quantity slas:(NSMutableArray *) slas {
    
    if(self = [super init]) {
        
        self.itemIndex = itemIndex;
        self.stockBalance = stockBalance;
        self.quantity = quantity;
        self.slas = slas;
        self.selectedSla = nil;
        self.lockTTL = nil;
        self.shippingEstimate = nil;
        self.price = 0;
        self.isSlaSelected = NO;
    }
    
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        NSData *jsonData;
        NSArray *jsonArray;
        
        self.slas = [[NSMutableArray alloc] init];
        
        if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        if(![JsonHelper getJsonData:&jsonData fromDictionary:jsonDictionary withJsonKey:_logisticsInfoSlas]) return self;
        if(![JsonHelper getJsonArray:&jsonArray fromJsonData:jsonData]) return self;
        
        for(NSDictionary *item in jsonArray) {
            
            NSData *jsonData;
            if(![JsonHelper getJsonData:&jsonData fromDictionary:item]) continue;
            
            [self.slas addObject:[[Sla alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]]];
        }
        
        self.itemIndex = [[jsonDictionary objectForKey:_logisticsInfoItemIndex] intValue];
        self.stockBalance = [[jsonDictionary objectForKey:_logisticsInfoStockBalance] intValue];
        self.quantity = [[jsonDictionary objectForKey:_logisticsInfoQuantity] intValue];
        self.selectedSla = nil;
        self.lockTTL = nil;
        self.shippingEstimate = nil;
        self.price = 0;
        self.isSlaSelected = NO;
    }
    
    return self;
}

- (void) selectSlaWithName:(NSString *) selectedSla lockTTL:(NSString *) lockTTL shippingEstimate:(NSString *) shippingEstimate andPrice:(long) price {
    
    self.selectedSla = selectedSla;
    self.lockTTL = lockTTL;
    self.shippingEstimate = shippingEstimate;
    self.price = price;
    self.isSlaSelected = YES;
}

- (NSDictionary *) toJsonObject {
    
    return [[NSDictionary alloc] initWithObjectsAndKeys:
            [NSNumber numberWithInt:[self itemIndex]], _logisticsInfoItemIndex,
            [self selectedSla], _logisticsInfoSelectedSla,
            [self lockTTL], _slaLockTTL,
            [self shippingEstimate], _slaShippingEstimate,
            [NSNumber numberWithLong:[self price]], _slaPrice, nil];
}

@end
