//
//  LogisticsInfo.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 28/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LogisticsInfo : NSObject

@property (nonatomic) int itemIndex;
@property (nonatomic) int stockBalance;
@property (nonatomic) int quantity;
@property (nonatomic, strong) NSMutableArray *slas;
@property (nonatomic, strong) NSString *selectedSla;
@property (nonatomic, strong) NSString *lockTTL;
@property (nonatomic, strong) NSString *shippingEstimate;
@property (nonatomic) long price;
@property (nonatomic) BOOL isSlaSelected;

- (id) initWithItemIndex:(int) itemIndex stockBalance:(int) stockBalance quantity:(int) quantity slas:(NSMutableArray *) slas;
- (id) initWithJsonString:(NSString *) jsonString;

- (void) selectSlaWithName:(NSString *) selectedSla lockTTL:(NSString *) lockTTL shippingEstimate:(NSString *) shippingEstimate andPrice:(long) price;
- (NSDictionary *) toJsonObject;

@end
