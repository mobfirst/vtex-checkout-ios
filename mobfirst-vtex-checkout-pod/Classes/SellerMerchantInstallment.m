//
//  SellerMerchantInstallment.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 28/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "SellerMerchantInstallment.h"
#import "JsonKeys.h"
#import "JsonHelper.h"

@implementation SellerMerchantInstallment

- (id) initWithId:(NSString *) id count:(int) count hasInterestRate:(BOOL) hasInterestRate interestRate:(long) interestRate value:(long) value andTotal:(long) total {
    
    if(self = [super init]) {
        
        self.id = id;
        self.count = count;
        self.hasInterestRate = hasInterestRate;
        self.interestRate = interestRate;
        self.value = value;
        self.total = total;
    }
    
    return self;
}
- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
    
        self.id = [jsonDictionary objectForKey:_sellerMerchantInstallmentId];
        self.count = [[jsonDictionary objectForKey:_sellerMerchantInstallmentCount] intValue];
        self.hasInterestRate = [[jsonDictionary objectForKey:_sellerMerchantInstallmentHasInterestRate] boolValue];
        self.interestRate = [[jsonDictionary objectForKey:_sellerMerchantInstallmentInterestRate] longValue];
        self.value = [[jsonDictionary objectForKey:_sellerMerchantInstallmentValue] longValue];
        self.total = [[jsonDictionary objectForKey:_sellerMerchantInstallmentTotal] longValue];
    }
    
    return self;
}

@end
