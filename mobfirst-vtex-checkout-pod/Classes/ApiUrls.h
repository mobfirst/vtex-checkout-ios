//
//  ApiUrls.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 31/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#ifndef ApiUrls_h
#define ApiUrls_h

// Checkout URLs
#define _simulateUrl @"http://%@.vtexcommercestable.com.br/api/checkout/pub/orderForms/simulation"
#define _placeOrderUrl @"http://%@.vtexcommercestable.com.br/api/checkout/pub/orders"
#define _gatewayCallbackUrl @"http://%@.vtexcommercestable.com.br/api/checkout/pub/gatewayCallback/%@"

// Payment URLs
#define _installmentsUrl @"https://%@.vtexpayments.com.br/api/pvt/installments"
#define _sendPaymentUrl @"https://%@.vtexpayments.com.br/api/pub/transactions/%@/payments"
#define _paymentSystemsUrl @"https://%@.vtexpayments.com.br/api/pvt/merchants/payment-systems"

// Master Data URLs
#define _getUserProfileUrl @"http://api.vtexcrm.com.br/%@/dataentities/CL/search"
#define _updateUserProfileUrl @"http://api.vtexcrm.com.br/%@/dataentities/CL/documents/%@"
#define _getUserAddressesUrl @"http://api.vtexcrm.com.br/%@/dataentities/AD/search"
#define _updateUserAddressUrl @"http://api.vtexcrm.com.br/%@/dataentities/AD/documents/%@"
#define _createUserAddressUrl @"http://api.vtexcrm.com.br/%@/dataentities/AD/documents"

#endif /* ApiUrls_h */
