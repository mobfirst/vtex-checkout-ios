//
//  StorageConstants.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 07/11/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#ifndef StorageConstants_h
#define StorageConstants_h

#define POSTAL_CODE_KEY @"POSTAL_CODE"
#define COUNTRY_KEY @"COUNTRY"

#define SLA_ID_KEY @"SLA_ID"
#define PAYMENT_SYSTEM_ID_KEY @"PAYMENT_SYSTEM"
#define INSTALLMENT_COUNT_KEY @"INSTALLMENT_COUNT"

#define USER_PROFILE_KEY @"USER_PROFILE"
#define USER_ADDRESS_KEY @"USER_ADDRESS"

#define CREDIT_CARD_KEY @"CREDIT_CARD"

#endif /* StorageConstants_h */
