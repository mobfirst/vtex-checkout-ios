//
//  UserProfile.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 30/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "UserProfile.h"
#import "JsonKeys.h"
#import "JsonHelper.h"

@implementation UserProfile

- (id) initWithId:(NSString *) id email:(NSString *) email firstName:(NSString *) firstName lastName:(NSString *) lastName document:(NSString *) document documentType:(NSString *) documentType andPhone:(NSString *) phone {
    
    if(self = [super init]) {
        
        self.id = id;
        self.email = email;
        self.firstName = firstName;
        self.lastName = lastName;
        self.document = document;
        self.documentType = documentType;
        self.phone = phone;
    }
    
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        self.id = [jsonDictionary objectForKey:_profileId];
        self.email = [jsonDictionary objectForKey:_profileEmail];
        self.firstName = [jsonDictionary objectForKey:_profileFirstName];
        self.lastName = [jsonDictionary objectForKey:_profileLastName];
        self.document = [jsonDictionary objectForKey:_profileDocument];
        self.documentType = [jsonDictionary objectForKey:_profileDocumentType];
        self.phone = [jsonDictionary objectForKey:_profilePhone];
    }
    
    return self;
}

- (NSDictionary *) toJsonObject {
    
    if([self id]) {
        
        return [[NSDictionary alloc] initWithObjectsAndKeys:
                [self id], _profileId,
                [self email], _profileEmail,
                [self firstName], _profileFirstName,
                [self lastName], _profileLastName,
                [self document], _profileDocument,
                [self documentType], _profileDocumentType,
                [self phone], _profilePhone, nil];
    }
    else {
        
        return [[NSDictionary alloc] initWithObjectsAndKeys:
                [self email], _profileEmail,
                [self firstName], _profileFirstName,
                [self lastName], _profileLastName,
                [self document], _profileDocument,
                [self documentType], _profileDocumentType,
                [self phone], _profilePhone, nil];
    }
}

@end
