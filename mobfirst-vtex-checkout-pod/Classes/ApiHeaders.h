//
//  ApiHeaders.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 31/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#ifndef ApiHeaders_h
#define ApiHeaders_h

// Header keys
#define _acceptHeaderKey @"Accept"
#define _contentTypeHeaderKey @"Content-Type"
#define _apiKeyHeaderKey @"X-VTEX-API-AppKey"
#define _apiTokenHeaderKey @"X-VTEX-API-AppToken"

// Header values
#define _acceptHeaderValue @"application/json"
#define _contentTypeHeaderValue @"application/json"

#endif /* ApiHeaders_h */
