//
//  CheckoutApi.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 28/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "CheckoutApi.h"
#import "ApiHeaders.h"
#import "SimulationBody.h"
#import "ApiUrls.h"
#import "JsonHelper.h"
#import "PaymentApi.h"
#import "OrderBody.h"
#import "PlaceOrderReturnObject.h"
#import "Order.h"
#import "InstallmentOption.h"
#import "OneClickCheckoutStorage.h"

@implementation CheckoutApi

- (id) initWithAccountName:(NSString *) accountName vtexApiKey:(NSString *) vtexApiKey andVtexApiToken:(NSString *) vtexApiToken {
    
    if(self = [super init]) {
        
        self.headers = [[NSDictionary alloc] initWithObjectsAndKeys:
                        _acceptHeaderValue, _acceptHeaderKey,
                        _contentTypeHeaderValue, _contentTypeHeaderKey,
                        vtexApiKey, _apiKeyHeaderKey,
                        vtexApiToken, _apiTokenHeaderKey, nil];
        
        self.accountName = accountName;
        self.vtexApiKey = vtexApiKey;
        self.vtexApiToken = vtexApiToken;
    }
    
    return self;
}

- (void) simulateWithItems:(NSArray *) items postalCode:(NSString *) postalCode country:(NSString *) country andCompletionHandler:(void (^)(SimulationReturnObject *simulationReturnObject, ErrorType error)) completionHandler {
    
    if(items && postalCode && country) {
        if([items count] > 0) {
            
            NSData *bodyData;
            
            NSDictionary *bodyDictionary = [[[SimulationBody alloc] initWithItems:items postalCode:postalCode andCountry:country] toJsonObject];
            if([JsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
                
                [HttpCheckoutConnection doPostToUrl:[NSString stringWithFormat:_simulateUrl, [self accountName]] withHeaders:[self headers] parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    
                    if(!error && response) {
                        if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                            
                            SimulationReturnObject *simulationReturnObject = [[SimulationReturnObject alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                            
                            if(simulationReturnObject) {
                                
                                [OneClickCheckoutStorage setSimulationPostalCode:postalCode];
                                [OneClickCheckoutStorage setSimulationCountry:country];
                                
                                completionHandler(simulationReturnObject, NONE);
                            }
                            else completionHandler(nil, RESPONSE_ERROR);
                        }
                        else completionHandler(nil, RESPONSE_ERROR);
                    }
                    else completionHandler(nil, COMMUNICATION_ERROR);
                }];
            }
            else completionHandler(nil, JSON_ERROR);
        }
        else completionHandler(nil, PARAM_VALIDATION_ERROR);
    }
    else completionHandler(nil, PARAM_ERROR);
}

- (void) placeOrderWithSimulationReturnObject:(SimulationReturnObject *) simulationReturnObject userProfile:(UserProfile *) userProfile userAddress:(UserAddress *) userAddress andCompletionHandler:(void (^)(PlaceOrderReturnObject *placeOrderReturnObject, ErrorType error)) completionHandler {
    
    if(simulationReturnObject && userProfile && userAddress) {
        if([simulationReturnObject validate]) {
            
            PaymentApi *paymentApi = [[PaymentApi alloc] initWithAccountName:[self accountName] vtexApiKey:[self vtexApiKey] andVtexApiToken:[self vtexApiToken]];
            [paymentApi getValueWithInterestFromSimulationReturnObject:simulationReturnObject withCompletionHandler:^(long valueWithInterest, long referenceValue, ErrorType error) {
                
                if(error == NONE) {
                    
                    NSData *bodyData;
                    
                    NSDictionary *bodyDictionary = [[[OrderBody alloc] initWithSimulationReturnObject:simulationReturnObject valueWithInterest:valueWithInterest referenceValue:referenceValue userProfile:userProfile andUserAddress:userAddress] toJsonObject];
                    
                    if([JsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
                        
                        [HttpCheckoutConnection doPutToUrl:[NSString stringWithFormat:_placeOrderUrl, [self accountName]] withHeaders:[self headers] parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                            
                            if(!error && response) {
                                if([((NSHTTPURLResponse *) response) statusCode] == 201) {
                                    
                                    PlaceOrderReturnObject *placeOrderReturnObject = [[PlaceOrderReturnObject alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                                    
                                    if(placeOrderReturnObject) {
                                        
                                        InstallmentOption *installmentOption = [[[simulationReturnObject paymentData] installmentOptions] objectAtIndex:0];
                                        [placeOrderReturnObject setInstallmentData:[installmentOption selectedInstallmentCount] interestRate:[installmentOption selectedInterestRate]];
                                        
                                        [OneClickCheckoutStorage setSlaId:[simulationReturnObject selectedSlaId]];
                                        [OneClickCheckoutStorage setPaymentSystemId:[simulationReturnObject selectedPaymentSystem]];
                                        [OneClickCheckoutStorage setInstallmentCount:[simulationReturnObject selectedInstallmentCount]];
                                        
                                        [OneClickCheckoutStorage setUserProfile:userProfile];
                                        [OneClickCheckoutStorage setUserAddress:userAddress];
                                        
                                        completionHandler(placeOrderReturnObject, NONE);
                                    }
                                    else completionHandler(nil, RESPONSE_ERROR);
                                }
                                else completionHandler(nil, RESPONSE_ERROR);
                            }
                            else completionHandler(nil, COMMUNICATION_ERROR);
                        }];
                    }
                    else completionHandler(nil, JSON_ERROR);
                }
                else completionHandler(nil, error);
            }];
        }
        else completionHandler(nil, PARAM_VALIDATION_ERROR);
    }
    else completionHandler(nil, PARAM_ERROR);
}

- (void) gatewayCallbackWithPlaceOrderReturnObject:(PlaceOrderReturnObject *) placeOrderReturnObject andCompletionHandler:(void (^)(ErrorType error)) completionHandler {
    
    if(placeOrderReturnObject) {
        if([placeOrderReturnObject validate]) {
            
            [HttpCheckoutConnection doPostToUrl:[NSString stringWithFormat:_gatewayCallbackUrl, [self accountName], [[[placeOrderReturnObject orders] objectAtIndex:0] orderGroup]] withHeaders:[self headers] parameters:nil body:nil andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                if(!error && response) {
                    
                    if([((NSHTTPURLResponse *) response) statusCode] == 204) completionHandler(NONE);
                    else completionHandler(RESPONSE_ERROR);
                }
                else completionHandler(COMMUNICATION_ERROR);
            }];
        }
        else completionHandler(PARAM_VALIDATION_ERROR);
    }
    else completionHandler(PARAM_ERROR);
}

- (void) oneClickCheckoutWithItems:(NSArray *) items andCompletionHandler:(void (^)(ErrorType error)) completionHandler {
    
    NSString *postalCode;
    NSString *country;
    __block NSString *slaId;
    __block NSString *paymentSystem;
    __block UserProfile *userProfile;
    __block UserAddress *userAddress;
    __block int installmentCount;
    
    if([OneClickCheckoutStorage getSimulationPostalCode:&postalCode] &&
       [OneClickCheckoutStorage getSimulationCountry:&country] &&
       [OneClickCheckoutStorage getSlaId:&slaId] &&
       [OneClickCheckoutStorage getPaymentSystemId:&paymentSystem] &&
       [OneClickCheckoutStorage getInstallmentCount:&installmentCount] &&
       [OneClickCheckoutStorage getUserProfile:&userProfile] &&
       [OneClickCheckoutStorage getUserAddress:&userAddress]) {
        
        __block CreditCard *creditCard;
        [OneClickCheckoutStorage getCreditCard:&creditCard];
        
        [self simulateWithItems:items postalCode:postalCode country:country andCompletionHandler:^(SimulationReturnObject *simulationReturnObject, ErrorType error) {
            
            if(error == NONE) {
                
                SimulationReturnObject *newSimulationReturnObject = [simulationReturnObject selectInstallmentsWithPaymentSystem:paymentSystem count:installmentCount];
                for(int i = 0; i < [[simulationReturnObject items] count]; i++) {
                    newSimulationReturnObject = [newSimulationReturnObject selectSla:slaId forItemIndex:i];
                }
                
                [self placeOrderWithSimulationReturnObject:newSimulationReturnObject userProfile:userProfile userAddress:userAddress andCompletionHandler:^(PlaceOrderReturnObject *placeOrderReturnObject, ErrorType error) {
                    
                    if(error == NONE) {
                        
                        PaymentApi *paymentApi = [[PaymentApi alloc] initWithAccountName:[self accountName] vtexApiKey:[self vtexApiKey] andVtexApiToken:[self vtexApiToken]];
                        [paymentApi sentPaymentWithPlaceOrderReturnObject:placeOrderReturnObject creditCard:creditCard andCompletionHandler:^(ErrorType error) {
                            
                            completionHandler(error);
                        }];
                    }
                    else completionHandler(error);
                }];
            }
            else completionHandler(error);
        }];
    }
    else completionHandler(PARAM_VALIDATION_ERROR);
}

@end
