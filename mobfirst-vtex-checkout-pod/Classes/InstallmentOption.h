//
//  InstallmentOption.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 28/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstallmentOption : NSObject

@property (nonatomic, strong) NSString *paymentSystem;
@property (nonatomic, strong) NSString *bin;
@property (nonatomic, strong) NSString *paymentName;
@property (nonatomic, strong) NSString *paymentGroupName;
@property (nonatomic) long value;
@property (nonatomic, strong) NSMutableArray *installments;
@property (nonatomic) int selectedInstallmentCount;
@property (nonatomic) long selectedInterestRate;
@property (nonatomic) long valueWithInterest;
@property (nonatomic) long referenceValue;
@property (nonatomic) BOOL isInstallmentCountSelected;

- (id) initWithPaymentSystem:(NSString *) paymentSystem bin:(NSString *) bin paymentName:(NSString *) paymentName paymentGroupName:(NSString *) paymentGroupName value:(long) value installments:(NSMutableArray *) installments;
- (id) initWithJsonString:(NSString *) jsonString;

- (void) selectInstallmentCount:(int) count;
- (void) setValueWithInterest:(long) valueWithInterest;
- (void) setReferenceValue:(long) referenceValue;
- (NSDictionary *) toJsonObject;

@end
