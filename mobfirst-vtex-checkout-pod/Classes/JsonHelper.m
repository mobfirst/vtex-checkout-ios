//
//  JsonHelper.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 30/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "JsonHelper.h"

@implementation JsonHelper

+ (BOOL) getDictionary:(NSDictionary **) dictionary fromJsonString:(NSString *) jsonString {
    
    NSError *error;
    *dictionary = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    if(!dictionary || error) return NO;
    return YES;
}

+ (BOOL) getJsonData:(NSData **) jsonData fromDictionary:(NSDictionary *) dictionary withJsonKey:(NSString *) key {
    
    NSError *error;
    *jsonData = [NSJSONSerialization dataWithJSONObject:[dictionary objectForKey:key] options:NSJSONWritingPrettyPrinted error:&error];
    
    if(!jsonData || error) return NO;
    return YES;
}

+ (BOOL) getJsonData:(NSData **) jsonData fromArray:(NSArray *) array {
    
    NSError *error;
    *jsonData = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&error];
    
    if(!jsonData || error) return NO;
    return YES;
}

+ (BOOL) getJsonData:(NSData **) jsonData fromDictionary:(NSDictionary *) dictionary {
    
    NSError *error;
    *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
    
    if(!jsonData || error) return NO;
    return YES;
}

+ (BOOL) getJsonArray:(NSArray **) jsonArray fromJsonData:(NSData *) jsonData {
    
    NSError *error;
    *jsonArray = [NSJSONSerialization JSONObjectWithData: jsonData options:kNilOptions error:&error];
    
    if(!jsonArray || error) return NO;
    return YES;
}

@end
