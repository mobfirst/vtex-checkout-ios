//
//  Message.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 29/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "Message.h"
#import "JsonKeys.h"
#import "JsonHelper.h"

@implementation Message

- (id) initWithCode:(NSString *) code text:(NSString *) text andStatus:(NSString *) status {
    
    if(self = [super init]) {
        
        self.code = code;
        self.text = text;
        self.status = status;
    }
    
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        self.code = [jsonDictionary objectForKey:_messageCode];
        self.text = [jsonDictionary objectForKey:_messageText];
        self.status = [jsonDictionary objectForKey:_messageStatus];
    }
    
    return self;
}

@end
