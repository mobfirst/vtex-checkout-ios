//
//  Order.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 29/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "Order.h"
#import "JsonKeys.h"
#import "JsonHelper.h"

@implementation Order

- (id) initWithOrderId:(NSString *) orderId andOrderGroup:(NSString *) orderGroup {
    
    if(self = [super init]) {
        
        self.orderId = orderId;
        self.orderGroup = orderGroup;
    }
    
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        self.orderId = [jsonDictionary objectForKey:_orderId];
        self.orderGroup = [jsonDictionary objectForKey:_orderGroup];
    }
    
    return self;
}

@end
