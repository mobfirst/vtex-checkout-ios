//
//  Item.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 28/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "Item.h"
#import "JsonKeys.h"
#import "JsonHelper.h"

@implementation Item

- (id) initWithId:(NSString *) id quantity:(int) quantity andSeller:(NSString *) seller {
    
    if(self = [super init]) {
        
        self.id = id;
        self.quantity = quantity;
        self.seller = seller;
    }
    
    return self;
}

- (id) initWithId:(NSString *) id quantity:(int) quantity seller:(NSString *) seller andPrice:(long) price {
    
    if(self = [super init]) {
        
        self.id = id;
        self.quantity = quantity;
        self.seller = seller;
        self.price = price;
    }
    
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        self.id = [jsonDictionary objectForKey:_itemId];
        self.quantity = [[jsonDictionary objectForKey:_itemQuantity] intValue];
        self.seller = [jsonDictionary objectForKey:_itemSeller];
        self.price = [[jsonDictionary objectForKey:_itemPrice] longValue];
    }
    
    return self;
}

- (NSDictionary *) toJsonObject {
    
    return [[NSDictionary alloc] initWithObjectsAndKeys:
            [self id], _itemId,
            [NSNumber numberWithInt:[self quantity]], _itemQuantity,
            [self seller], _itemSeller,
            [NSNumber numberWithLong:[self price]], _itemPrice, nil];
}

@end
