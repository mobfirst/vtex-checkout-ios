//
//  CreditCard.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 30/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "CreditCard.h"
#import "JsonKeys.h"
#import "JsonHelper.h"

@implementation CreditCard

- (id) initWithHolderName:(NSString *) holderName cardNumber:(NSString *) cardNumber validationCode:(NSString *) validationCode dueDate:(NSString *) dueDate document:(NSString *) document accountId:(NSString *) accountId address:(NSString *) address callbackUrl:(NSString *) callbackUrl {
    
    if(self = [super init]) {
        
        self.holderName = holderName;
        self.cardNumber = cardNumber;
        self.validationCode = validationCode;
        self.dueDate = dueDate;
        self.document = document;
        self.accountId = accountId;
        self.address = address;
        self.callbackUrl = callbackUrl;
    }
    
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        self.holderName = [jsonDictionary objectForKey:_creditCardHolderName];
        self.cardNumber = [jsonDictionary objectForKey:_creditCardNumber];
        self.validationCode = [jsonDictionary objectForKey:_creditCardValidationCode];
        self.dueDate = [jsonDictionary objectForKey:_creditCardDueDate];
        self.document = [jsonDictionary objectForKey:_creditCardDocument];
        self.accountId = [jsonDictionary objectForKey:_creditCardAccountId];
        self.address = [jsonDictionary objectForKey:_creditCardAddress];
        self.callbackUrl = [jsonDictionary objectForKey:_creditCardCallbackUrl];
    }
    
    return self;
}

- (NSDictionary *) toJsonObject {
    
    return [[NSDictionary alloc] initWithObjectsAndKeys:
            [self holderName], _creditCardHolderName,
            [self cardNumber], _creditCardNumber,
            [self validationCode], _creditCardValidationCode,
            [self dueDate], _creditCardDueDate,
            [self document], _creditCardDocument,
            [self accountId], _creditCardAccountId,
            [self address], _creditCardAddress,
            [self callbackUrl], _creditCardCallbackUrl, nil];
}

@end
