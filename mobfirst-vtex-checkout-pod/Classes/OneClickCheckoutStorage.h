//
//  OneClickCheckoutStorage.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 07/11/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserAddress.h"
#import "UserProfile.h"
#import "CreditCard.h"

@interface OneClickCheckoutStorage : NSObject

+ (BOOL) getSimulationPostalCode:(NSString **) postalCode;
+ (BOOL) getSimulationCountry:(NSString **) simulationCountry;
+ (BOOL) getSlaId:(NSString **) slaId;
+ (BOOL) getPaymentSystemId:(NSString **) paymentSystemId;
+ (BOOL) getInstallmentCount:(int *) installmentCount;
+ (BOOL) getUserProfile:(UserProfile **) userProfile;
+ (BOOL) getUserAddress:(UserAddress **) userAddress;
+ (BOOL) getCreditCard:(CreditCard **) creditCard;

+ (BOOL) setSimulationPostalCode:(NSString *) postalCode;
+ (BOOL) setSimulationCountry:(NSString *) simulationCountry;
+ (BOOL) setSlaId:(NSString *) slaId;
+ (BOOL) setPaymentSystemId:(NSString *) paymentSystemId;
+ (BOOL) setInstallmentCount:(int) installmentCount;
+ (BOOL) setUserProfile:(UserProfile *) userProfile;
+ (BOOL) setUserAddress:(UserAddress *) userAddress;
+ (BOOL) setCreditCard:(CreditCard *) creditCard;

@end
