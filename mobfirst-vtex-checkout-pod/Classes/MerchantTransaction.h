//
//  MerchantTransaction.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 29/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MerchantTransaction : NSObject

@property (nonatomic, strong) NSString *transactionId;
@property (nonatomic, strong) NSString *merchantName;
@property (nonatomic, strong) NSMutableArray *payments;

- (id) initWithTransactionId:(NSString *)transactionId merchantName:(NSString *) merchantName andPayments:(NSMutableArray *) payments;
- (id) initWithJsonString:(NSString *) jsonString;

- (NSDictionary *) toJsonObject;

@end
