//
//  Error.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 28/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Error : NSObject

@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *exception;

- (id) initWithCode:(NSString *) code message:(NSString *) message andExsception:(NSString *) exception;
- (id) initWithJsonString:(NSString *) jsonString;

@end
