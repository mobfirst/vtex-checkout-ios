//
//  CreditCard.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 30/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CreditCard : NSObject

@property (nonatomic, strong) NSString *holderName;
@property (nonatomic, strong) NSString *cardNumber;
@property (nonatomic, strong) NSString *validationCode;
@property (nonatomic, strong) NSString *dueDate;
@property (nonatomic, strong) NSString *document;
@property (nonatomic, strong) NSString *accountId;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *callbackUrl;

- (id) initWithHolderName:(NSString *) holderName cardNumber:(NSString *) cardNumber validationCode:(NSString *) validationCode dueDate:(NSString *) dueDate document:(NSString *) document accountId:(NSString *) accountId address:(NSString *) address callbackUrl:(NSString *) callbackUrl;
- (id) initWithJsonString:(NSString *) jsonString;

- (NSDictionary *) toJsonObject;

@end
