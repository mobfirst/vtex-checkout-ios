//
//  SimulationBody.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 30/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "SimulationBody.h"
#import "JsonKeys.h"
#import "Item.h"

@implementation SimulationBody

- (id) initWithItems:(NSArray *) items postalCode:(NSString *) postalCode andCountry:(NSString *) country {
    
    if(self = [super init]) {
        
        self.items = items;
        self.postalCode = postalCode;
        self.country = country;
    }
    
    return self;
}

- (NSDictionary *) toJsonObject {
    
    NSMutableArray *itemsArray = [[NSMutableArray alloc] init];
    for(Item *item in [self items]) [itemsArray addObject:[item toJsonObject]];
    
    return [[NSDictionary alloc] initWithObjectsAndKeys:
            itemsArray, _simulationItems,
            [self postalCode], _simulationPostalCode,
            [self country], _simulationCountry, nil];
}

@end
