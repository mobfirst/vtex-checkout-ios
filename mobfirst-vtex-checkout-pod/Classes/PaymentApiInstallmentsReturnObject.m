//
//  PaymentApiInstallmentsReturnObject.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 30/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "PaymentApiInstallmentsReturnObject.h"
#import "JsonKeys.h"
#import "JsonHelper.h"
#import "PaymentApiInstallment.h"
#import "PaymentApiOption.h"

@implementation PaymentApiInstallmentsReturnObject

- (id) initWithValue:(long) value andInstallments:(NSMutableArray *) installments {
    
    if(self = [super init]) {
        
        self.value = value;
        self.installments = installments;
    }
    
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        NSData *jsonData;
        NSArray *jsonArray;
        
        self.installments = [[NSMutableArray alloc] init];
        
        if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        if(![JsonHelper getJsonData:&jsonData fromDictionary:jsonDictionary withJsonKey:_paymentApiInstallmentsReturnObjectInstallments]) return self;
        if(![JsonHelper getJsonArray:&jsonArray fromJsonData:jsonData]) return self;
        
        for(NSDictionary *item in jsonArray) {
            
            NSData *jsonData;
            if(![JsonHelper getJsonData:&jsonData fromDictionary:item]) continue;
            
            [self.installments addObject:[[PaymentApiInstallment alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]]];
        }
        
        self.value = [[jsonDictionary objectForKey:_paymentApiInstallmentsReturnObjectValue] longValue];
    }
    
    return self;
}

- (BOOL) valueWithInterest:(long *) value forInstallmentCount:(int) installmentCount {
    
    if([[self installments] count] == 1) {
        
        PaymentApiInstallment *installment = [[self installments] objectAtIndex:0];
        for(PaymentApiOption *option in [installment options]) {
            
            if([option quantity] == installmentCount) {
                
                *value = (long)([option value] * [option quantity]);
                return YES;
            }
        }
    }
    
    return NO;
}

@end
