//
//  TransactionData.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 29/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransactionData : NSObject

@property (nonatomic, strong) NSMutableArray *merchantTransactions;

- (id) initWithMerchantTransactions:(NSMutableArray *) merchantTransactions;
- (id) initWithJsonString:(NSString *) jsonString;

@end
