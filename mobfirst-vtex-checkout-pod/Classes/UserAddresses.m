//
//  UserAddresses.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 30/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "UserAddresses.h"
#import "JsonHelper.h"
#import "UserAddress.h"

@implementation UserAddresses

- (id) initWithUserAddresses:(NSMutableArray *) addresses {
    
    if(self = [super init]) self.addresses = addresses;
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        NSData *jsonData;
        NSArray *jsonArray;
        
        if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        if(![JsonHelper getJsonData:&jsonData fromDictionary:jsonDictionary]) return self;
        if(![JsonHelper getJsonArray:&jsonArray fromJsonData:jsonData]) return self;
        
        for(NSDictionary *item in jsonArray) {
            
            NSData *jsonData;
            if(![JsonHelper getJsonData:&jsonData fromDictionary:item]) continue;
            
            [self.addresses addObject:[[UserAddress alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]]];
        }
    }
    
    return self;
}

@end
