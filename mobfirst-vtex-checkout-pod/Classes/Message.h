//
//  Message.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 29/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Message : NSObject

@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *status;

- (id) initWithCode:(NSString *) code text:(NSString *) text andStatus:(NSString *) status;
- (id) initWithJsonString:(NSString *) jsonString;

@end
