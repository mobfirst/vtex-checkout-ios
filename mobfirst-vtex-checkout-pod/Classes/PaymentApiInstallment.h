//
//  PaymentApiInstallment.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 30/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaymentApiInstallment : NSObject

@property (nonatomic, strong) NSMutableArray *options;

- (id) initWithJsonString:(NSString *) jsonString;

@end
