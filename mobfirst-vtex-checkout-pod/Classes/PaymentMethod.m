//
//  PaymentMethod.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 06/11/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "PaymentMethod.h"
#import "JsonKeys.h"
#import "JsonHelper.h"

@implementation PaymentMethod

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        self.id = [[jsonDictionary objectForKey:_paymentMethodId] intValue];
        self.name = [jsonDictionary objectForKey:_paymentMethodName];
        self.isAvailable = [[jsonDictionary objectForKey:_paymentMethodIsAvailable] boolValue];
    }
    
    return self;
}

@end
