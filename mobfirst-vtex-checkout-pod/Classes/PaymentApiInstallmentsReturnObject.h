//
//  PaymentApiInstallmentsReturnObject.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 30/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaymentApiInstallmentsReturnObject : NSObject

@property (nonatomic) long value;
@property (nonatomic, strong) NSMutableArray *installments;

- (id) initWithValue:(long) value andInstallments:(NSMutableArray *) installments;
- (id) initWithJsonString:(NSString *) jsonString;

- (BOOL) valueWithInterest:(long *) value forInstallmentCount:(int) installmentCount;

@end
