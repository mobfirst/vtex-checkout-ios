//
//  ApiParameters.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 31/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#ifndef ApiParameters_h
#define ApiParameters_h

// URL parameter keys
#define _userIdParameterKey @"userId"
#define _searchFieldsParameterKey @"_fields"
#define _installmentsValueParameterKey @"request.value"
#define _installmentsSalesChannelParameterKey @"request.salesChannel"
#define _installmentsPaymentMethodIdParameterKey @"request.paymentDetails[0].id"
#define _installmentsPaymentMethodValueParameterKey @"request.paymentDetails[0].value"

// Parameter values
#define _userFieldsParameterValue @"id,email,firstName,lastName,documentType,document,phone"
#define _addressesFieldsParameterValue @"id,street,number,neighborhood,postalCode,city,state,country,addressType,addressName,receiverName,complement,reference"

#endif /* ApiParameters_h */
