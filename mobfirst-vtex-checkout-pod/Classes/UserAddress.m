//
//  UserAddress.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 30/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "UserAddress.h"
#import "JsonKeys.h"
#import "JsonHelper.h"

@implementation UserAddress

- (id) initWithId:(NSString *) id addressType:(NSString *) addressType receiverName:(NSString *) receiverName addressId:(NSString *) addressId postalCode:(NSString *) postalCode city:(NSString *) city state:(NSString *) state country:(NSString *) country street:(NSString *) street number:(NSString *) number neighborhood:(NSString *) neighborhood complement:(NSString *) complement andReference:(NSString *) reference {
    
    if(self = [super init]) {
        
        self.id = id;
        self.addressType = addressType;
        self.receiverName = receiverName;
        self.addressId = addressId;
        self.postalCode = postalCode;
        self.city = city;
        self.state = state;
        self.country = country;
        self.street = street;
        self.number = number;
        self.neighborhood = neighborhood;
        self.complement = complement;
        self.reference = reference;
    }
    
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![JsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        self.id = [jsonDictionary objectForKey:_addressId];
        self.addressType = [jsonDictionary objectForKey:_addressType];
        self.receiverName = [jsonDictionary objectForKey:_addressReceiverName];
        self.addressId = [jsonDictionary objectForKey:_addressAddressId];
        self.postalCode = [jsonDictionary objectForKey:_addressPostalCode];
        self.city = [jsonDictionary objectForKey:_addressCity];
        self.state = [jsonDictionary objectForKey:_addressState];
        self.country = [jsonDictionary objectForKey:_addressCountry];
        self.street = [jsonDictionary objectForKey:_addressStreet];
        self.number = [jsonDictionary objectForKey:_addressNumber];
        self.neighborhood = [jsonDictionary objectForKey:_addressNeighborhood];
        self.complement = [jsonDictionary objectForKey:_addressComplement];
        self.reference = [jsonDictionary objectForKey:_addressReference];
    }
    
    return self;
}

- (NSDictionary *) toJsonObject {
    
    if([self id]) {
        
        return [[NSDictionary alloc] initWithObjectsAndKeys:
                [self id], _addressId,
                [self addressType], _addressType,
                [self receiverName], _addressReceiverName,
                [self addressId], _addressAddressId,
                [self postalCode], _addressPostalCode,
                [self city], _addressCity,
                [self state], _addressState,
                [self country], _addressCountry,
                [self street], _addressStreet,
                [self number], _addressNumber,
                [self neighborhood], _addressNeighborhood,
                [self complement], _addressComplement,
                [self reference], _addressReference, nil];
    }
    else {
        
        return [[NSDictionary alloc] initWithObjectsAndKeys:
                [self addressType], _addressType,
                [self receiverName], _addressReceiverName,
                [self addressId], _addressAddressId,
                [self postalCode], _addressPostalCode,
                [self city], _addressCity,
                [self state], _addressState,
                [self country], _addressCountry,
                [self street], _addressStreet,
                [self number], _addressNumber,
                [self neighborhood], _addressNeighborhood,
                [self complement], _addressComplement,
                [self reference], _addressReference, nil];
    }
}

@end
