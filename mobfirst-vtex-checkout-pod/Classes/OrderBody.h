//
//  OrderBody.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 30/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SimulationReturnObject.h"
#import "UserProfile.h"
#import "UserAddress.h"

@interface OrderBody : NSObject

@property (nonatomic, strong) SimulationReturnObject *simulationReturnObject;
@property (nonatomic) long valueWithInterest;
@property (nonatomic) long referenceValue;
@property (nonatomic, strong) UserProfile *userProfile;
@property (nonatomic, strong) UserAddress *userAddress;

- (id) initWithSimulationReturnObject:(SimulationReturnObject *) simulationReturnObject valueWithInterest:(long) valueWithInterest referenceValue:(long) referenceValue userProfile:(UserProfile *) userProfile andUserAddress:(UserAddress *) userAddress;

- (NSDictionary *) toJsonObject;

@end
