//
//  JsonValues.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 30/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#ifndef JsonValues_h
#define JsonValues_h

#define _shippingDataIdValue @"shippingData"
#define _paymentDataIdValue @"paymentData"

#endif /* JsonValues_h */
