//
//  Sla.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 28/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Sla : NSObject

@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *shippingEstimate;
@property (nonatomic, strong) NSString *lockTTL;
@property (nonatomic) long price;
@property (nonatomic) long listPrice;
@property (nonatomic) long tax;

- (id) initWithId:(NSString *) id name:(NSString *) name shippingEstimate:(NSString *) shippingEstimate lockTTL:(NSString *) lockTTL price:(long) price listPrice:(long) listPrice andTax:(long) tax;
- (id) initWithJsonString:(NSString *) jsonString;

@end
