//
//  MasterDataApi.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 28/08/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "MasterDataApi.h"
#import "ApiHeaders.h"
#import "ApiParameters.h"
#import "HttpCheckoutConnection.h"
#import "ApiUrls.h"
#import "ApiParameters.h"
#import "JsonHelper.h"

@implementation MasterDataApi

- (id) initWithAccountName:(NSString *) accountName vtexApiKey:(NSString *) vtexApiKey andVtexApiToken:(NSString *) vtexApiToken {
    
    if(self = [super init]) {
        
        self.headers = [[NSDictionary alloc] initWithObjectsAndKeys:
                        _acceptHeaderValue, _acceptHeaderKey,
                        _contentTypeHeaderValue, _contentTypeHeaderKey,
                        vtexApiKey, _apiKeyHeaderKey,
                        vtexApiToken, _apiTokenHeaderKey, nil];
        
        self.accountName = accountName;
        self.vtexApiKey = vtexApiKey;
        self.vtexApiToken = vtexApiToken;
    }
    
    return self;
}

- (void) getUserProfileWithUserId:(NSString *) userId andCompletionHandler:(void (^)(UserProfile *userProfile, ErrorType error)) completionHandler {
    
    if(userId) {
        
        NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                    userId, _userIdParameterKey,
                                    _userFieldsParameterValue, _searchFieldsParameterKey, nil];
        
        [HttpCheckoutConnection doGetFromUrl:[NSString stringWithFormat:_getUserProfileUrl, [self accountName]] withHeaders:[self headers] parameters:parameters andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(!error && response) {
                if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                    
                    UserProfile *userProfile = [[UserProfile alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                    
                    if(userProfile) completionHandler(userProfile, NONE);
                    else completionHandler(nil, RESPONSE_ERROR);
                }
                else completionHandler(nil, RESPONSE_ERROR);
            }
            else completionHandler(nil, COMMUNICATION_ERROR);
        }];
    }
    else completionHandler(nil, PARAM_ERROR);
}

- (void) updateUserProfile:(UserProfile *) oldUserProfile withNewUserProfile:(UserProfile *) newUserProfile andCompletionHandler:(void (^)(ErrorType error)) completionHandler {
    
    if(oldUserProfile && newUserProfile) {
        
        NSData *bodyData;
        NSDictionary *bodyDictionary = [newUserProfile toJsonObject];
        
        if([JsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
            
            [HttpCheckoutConnection doPatchToUrl:[NSString stringWithFormat:_updateUserProfileUrl, [self accountName], [oldUserProfile id]] withHeaders:[self headers] parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                if(!error && response) {
                    if([((NSHTTPURLResponse *) response) statusCode] == 204) completionHandler(NONE);
                    else completionHandler(RESPONSE_ERROR);
                }
                else completionHandler(COMMUNICATION_ERROR);
            }];
        }
        else completionHandler(JSON_ERROR);
    }
    else completionHandler(PARAM_ERROR);
}

- (void) getUserAddressesFromUserProfile:(UserProfile *) userProfile andCompletionHandler:(void (^)(UserAddresses *addresses, ErrorType error)) completionHandler {
    
    if(userProfile) {
        
        NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                    [userProfile id], _userIdParameterKey,
                                    _addressesFieldsParameterValue, _searchFieldsParameterKey, nil];
        
        [HttpCheckoutConnection doGetFromUrl:[NSString stringWithFormat:_getUserAddressesUrl, [self accountName]] withHeaders:[self headers] parameters:parameters andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(!error && response) {
                if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                    
                    UserAddresses *userAddresses = [[UserAddresses alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                    
                    if(userAddresses) completionHandler(userAddresses, NONE);
                    else completionHandler(nil, RESPONSE_ERROR);
                }
                else completionHandler(nil, RESPONSE_ERROR);
            }
            else completionHandler(nil, COMMUNICATION_ERROR);
        }];
    }
    else completionHandler(nil, PARAM_ERROR);
}

- (void) updateUserAddress:(UserAddress *) oldUserAddress withNewUserAddress:(UserAddress *) newUserAddress andCompletionHandler:(void (^)(ErrorType error)) completionHandler {
    
    if(oldUserAddress && newUserAddress) {
        
        NSData *bodyData;
        NSDictionary *bodyDictionary = [newUserAddress toJsonObject];
        
        if([JsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
            
            [HttpCheckoutConnection doPatchToUrl:[NSString stringWithFormat:_updateUserAddressUrl, [self accountName], [oldUserAddress id]] withHeaders:[self headers] parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                if(!error && response) {
                    if([((NSHTTPURLResponse *) response) statusCode] == 204) completionHandler(NONE);
                    else completionHandler(RESPONSE_ERROR);
                }
                else completionHandler(COMMUNICATION_ERROR);
            }];
        }
        else completionHandler(JSON_ERROR);
    }
    else completionHandler(PARAM_ERROR);
}

- (void) createUserAddressWithUserProfile:(UserProfile *) userProfile userAddress:(UserAddress *) userAddress andCompletionHandler:(void (^)(ErrorType error)) completionHandler {
    
    if(userProfile && userAddress) {
        if([userProfile id]) {
            
            NSData *bodyData;
            NSDictionary *bodyDictionary = [userAddress toJsonObject];
            
            if([JsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
                
                [HttpCheckoutConnection doPostToUrl:[NSString stringWithFormat:_createUserAddressUrl, [self accountName]] withHeaders:[self headers] parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    
                    if(!error && response) {
                        if([((NSHTTPURLResponse *) response) statusCode] == 201) completionHandler(NONE);
                        else completionHandler(RESPONSE_ERROR);
                    }
                    else completionHandler(COMMUNICATION_ERROR);
                }];
            }
            else completionHandler(JSON_ERROR);
        }
        else completionHandler(PARAM_VALIDATION_ERROR);
    }
    else completionHandler(PARAM_ERROR);
}

@end
