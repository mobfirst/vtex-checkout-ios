//
//  SimulationBody.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 30/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SimulationBody : NSObject

@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) NSString *postalCode;
@property (nonatomic, strong) NSString *country;

- (id) initWithItems:(NSArray *) items postalCode:(NSString *) postalCode andCountry:(NSString *) country;

- (NSDictionary *) toJsonObject;

@end
