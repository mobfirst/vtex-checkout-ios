//
//  UserProfile.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 30/10/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserProfile : NSObject

@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *document;
@property (nonatomic, strong) NSString *documentType;
@property (nonatomic, strong) NSString *phone;

- (id) initWithId:(NSString *) id email:(NSString *) email firstName:(NSString *) firstName lastName:(NSString *) lastName document:(NSString *) document documentType:(NSString *) documentType andPhone:(NSString *) phone;
- (id) initWithJsonString:(NSString *) jsonString;

- (NSDictionary *) toJsonObject;

@end
