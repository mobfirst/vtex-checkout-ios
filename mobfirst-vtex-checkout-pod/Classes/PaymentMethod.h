//
//  PaymentMethod.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 06/11/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaymentMethod : NSObject

@property (nonatomic) int id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic) BOOL isAvailable;

- (id) initWithJsonString:(NSString *) jsonString;

@end
